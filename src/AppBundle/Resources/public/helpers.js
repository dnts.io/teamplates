export {areYouSureHandler};

function areYouSureHandler() {
    $('a.confirm-it').click(function (e) {
        e.preventDefault();

        if (confirm('Êtes vous sur ?')) {
            window.location = $(this).attr('href');
        }
    });
}