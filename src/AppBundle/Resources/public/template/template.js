export {expandHandle};

function expandHandle() {

    $('.expand').click(function () {

        const $this = $(this),
            currentAction = $(this).attr('class').split(' ')[1];

        switch (currentAction) {
            case 'expand-tree' :
                expandTree($this);
                break;
            case 'reduce-tree' :
                reduceTree($this);
                break;
            case 'expand-editor':
                expandEditor($this);
                break;
            case 'reduce-editor' :
                reduceEditor($this);
                break;
        }
    });
}


function expandTree($this) {
    reduceTemplateSuggestionContainer();
    reduceEditorContainer();
    expandTreeContainer();

    $this
        .removeClass('expand-tree')
        .addClass('reduce-tree')
        .find('i').removeClass('fa-arrow-circle-left').addClass('fa-arrow-circle-right')
}

function reduceTree($this) {
    expandTemplateSuggestionContainer();
    reduceTreeContainer();

    $this
        .removeClass('reduce-tree')
        .addClass('expand-tree')
        .find('i').removeClass('fa-arrow-circle-right').addClass('fa-arrow-circle-left')
}

function expandEditor($this) {
    expandEditorContainer();
    reduceTemplateSuggestionContainer();
    reduceTreeContainer();

    $this
        .removeClass('expand-editor')
        .addClass('reduce-editor')
        .find('i').removeClass('fa-arrow-circle-left').addClass('fa-arrow-circle-right')
}

function reduceEditor($this) {
    expandTemplateSuggestionContainer();
    reduceTreeContainer();
    reduceEditorContainer();

    $this
        .removeClass('reduce-editor')
        .addClass('expand-editor')
        .find('i').removeClass('fa-arrow-circle-right').addClass('fa-arrow-circle-left')
}

function reduceTemplateSuggestionContainer() {
    $('#template-suggestion-container').hide();
}

function expandTemplateSuggestionContainer() {
    $('#template-suggestion-container').show();
}

function expandTreeContainer() {
    $('#template-tree-container').removeClass().addClass('col-md-8 col-lg-8')
}

function reduceTreeContainer() {
    $('#template-tree-container').removeClass().addClass('col-md-6 col-lg-4')
}

function expandEditorContainer() {
    $('#template-editor-container').removeClass().addClass('col-md-8 col-lg-8');
}

function reduceEditorContainer() {
    $('#template-editor-container').removeClass().addClass('col-md-6 col-lg-4')
}
