export {initDashboard};

function initDashboard() {
    handleSettingsClick();
}

function handleSettingsClick() {

    $('html').click(function () {
        $('.settings-links.open').removeClass('open');
    });

    $('.display-settings').click(function (e) {
        e.preventDefault();
        e.stopPropagation();

        const $settingsLink = $('.settings-links');
        $settingsLink[$settingsLink.hasClass('open') ? 'removeClass' : 'addClass']('open');
    })
}