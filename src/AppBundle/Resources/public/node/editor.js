export {load};

function load(e) {
    e.preventDefault();
    var nodeId = $(this).data('id');

    getNodeForm(nodeId)
        .then(function (resp) {
            $('#ckeditor-container').html(resp);
        });
}

function getNodeForm(nodeId) {
    return $.get(Routing.generate('node_get_content_form', {nodeId: nodeId}));
}
