

const glyphConfig = {
    map: {
        checkbox: "fa fa-square-o",
        checkboxSelected: "fa fa-check-square-o",
        checkboxUnknown: "fa fa-square",
        dragHelper: "fa fa-arrow-right",
        dropMarker: "fa fa-long-arrow-right",
        error: "fa fa-warning",
        expanderClosed: "fa fa-caret-right",
        expanderLazy: "fa fa-angle-right",
        expanderOpen: "fa fa-caret-down",
        loading: "fa fa-spinner fa-pulse"
    }
};

const dragNDropConfig = {
    autoExpandMS: 400,
    focusOnClick: true,
    preventVoidMoves: true, // Prevent dropping nodes 'before self', etc.
    preventRecursiveMoves: true, // Prevent dropping nodes on own descendants
    dragStart: function (node, data) {
        /** This function MUST be defined to enable dragging for the tree.
         *  Return false to cancel dragging of node.
         */
        return true;
    },
    dragEnter: function (node, data) {
        /** data.otherNode may be null for non-fancytree droppables.
         *  Return false to disallow dropping on node. In this case
         *  dragOver and dragLeave are not called.
         *  Return 'over', 'before, or 'after' to force a hitMode.
         *  Return ['before', 'after'] to restrict available hitModes.
         *  Any other return value will calc the hitMode from the cursor position.
         */
        // Prevent dropping a parent below another parent (only sort
        // nodes under the same parent)
        /*           if(node.parent !== data.otherNode.parent){
         return false;
         }
         // Don't allow dropping *over* a node (would create a child)
         return ["before", "after"];
         */
        return true;
    },
    dragDrop: function (node, data) {
        /** This function MUST be defined to enable dropping of items on
         *  the tree.
         */

        /*
        if data.hitMode == after/before/over
               node : the node after/before/over the one we drag
               data.otherNode : the node we drag

        ex:
        a
        b

        We drag a after b
        b
        a

        node : b
        data.otherNode : a

        */

        const draggedNodeId = data.otherNode.data._id,
            targetNodeId = node.data._id,
            hitMode = data.hitMode;

        console.log(draggedNodeId);
        console.log(targetNodeId);
        console.log(hitMode);

        data.otherNode.moveTo(node, data.hitMode);
    }
};


export {dragNDropConfig, glyphConfig};