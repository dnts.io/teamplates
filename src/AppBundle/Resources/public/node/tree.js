import * as node from "node/node";
import * as rule from "node/rule";
import * as editor from "node/editor";
import * as treeConfig from "node/tree-config";

export {initTree};

function initTree() {
    $("#treetable").fancytree({
        extensions: ["table", 'glyph', "edit", "dnd"],
        edit: {
            save: function (event, data) {

                saveNewNodeTitle(data.node.data._id, data.input.val())
                    .fail(function () {
                        node.setTitle(data.node.orgTitle);
                    });
            }
        },

        dnd: treeConfig.dragNDropConfig,
        glyph: treeConfig.glyphConfig,
        checkbox: false,
        table: {
            indentation: 20,      // indent 20px per node level
            nodeColumnIdx: 1     // render the node title into the first column
        },
        source: {
            url: Routing.generate('node_get_by_template', {templateId: $('#template-id').val()})
        },
        renderColumns: function (event, data) {
            var node = data.node,
                $tdList = $(node.tr).find(">td");


            $tdList.eq(0).html(`<a class="icon-box" href="#"><i class="fa fa-arrows"></i></a>`);
            // (index #2 is rendered by fancytree)
            var commonAttributes = " data-key='" + node.key + "' data-node-id='" + node.data._id + "' data-id='" + node.data._id + "'";

            var actionButtons = `
                <a class="icon-box node-view" ${commonAttributes} href="#" title="View content" ><span class="fa fa-eye" /></a>
                <a class="icon-box node-edit-title" ${commonAttributes} href="#" title="Edit title" ><span class="fa fa-pencil" /></a>
                <a class="icon-box add-child" ${commonAttributes} href="#" title="Add child" ><span class="fa fa-plus" /></a>
                <a class="icon-box node-remove" ${commonAttributes} href="#" title="Remove the node and children" ><span class="fa fa-remove" /></a>
            `;

            if (node.data.isLocked) {
                actionButtons += `<a class="icon-box" href="#" title="In progress. ${node.data.lock.username} since ${node.data.lock.startDate}" ><span class="fa fa-cog fa-spin fa-fw" /></a> `;
            } else {
                actionButtons += `<a class="icon-box node-load-editor" ${commonAttributes} href="#" title="Edit content"  ><span class="fa fa-edit" /></a> `;
            }

            if(node.data.validated) {
                actionButtons += `<a class="icon-box node-validate" ${commonAttributes} data-validated="1" href="#" title="Validated"  ><i class="fa fa-lock" /></a> `;
            } else {
                actionButtons += `<a class="icon-box node-validate" ${commonAttributes} data-validated="0" href="#" title="Not yet validated"  ><i class="fa fa-unlock" /></a> `;
            }

            actionButtons += `<a class="icon-box node-condition-access-modal" ${commonAttributes} href="#" title="Edit conditions"  ><i class="fa fa-universal-access " /></a> `;

            $tdList.eq(2).html(actionButtons);
        }
    });


    $('body')
        .on('click', '.node-edit-title', node.editTitle)
        .on('click', '.node-save-content', node.save)
        .on('click', '.node-view-content', node.viewRenderedNodeInModal)
        .on('click', '.node-view', node.viewRenderedNodeInModal)
        .on('click', '.node-edit-content', node.edit)
        .on('click', '.node-close-content', node.close)
        .on('click', '.node-validate', node.validate)
        .on('click', '.node-load-editor', editor.load)
        .on('click', '.add-child', node.addChild)
        .on('click', '.add-root', node.addRoot)
        .on('click', '.view-alpha', node.viewRenderedNodeInModal)
        .on('click', '.node-condition-access-modal', node.displayAccessModal)
        .on('click', '.node-remove', node.remove)
        .on('click', '.condition-add-one', rule.handleAddCondition)
    ;
}


function saveNewNodeTitle(nodeId, newTitle) {
    return $.post(Routing.generate('node_save_new_title', {nodeId: nodeId}), {title: newTitle});
}