export {create, handleAddCondition, removeAll};

function handleAddCondition() {

    const $newRule = $('.rule-line:first').clone();

    $('.rules-container').append($newRule);

    // put on default value
    $('select', $newRule).show().val(0).change();
    $('input', $newRule).val('');
}

function create() {

    const formSerialized = $('#form-node-rules').serialize(),
        nodeId = $('#node-id', '#form-node-rules').val();

    sendRules(formSerialized, nodeId)
        .then(function () {
            $("#access-modal").dialog('destroy').remove();
            alert('Les conditions ont été correctement enregistrées');
        })
        .catch(() => {
            alert('Une erreur est survenue durant la sauvegarde des conditions');
        });
}

function removeAll() {

    const nodeId = $('#node-id', '#form-node-rules').val();

    removeAllRules(nodeId)
        .then((resp) => {
            if (resp.success) {
                $("#access-modal").dialog('destroy').remove();
                alert('Les conditions ont été toutes supprimées');
            }
        })
        .catch(() => {
            alert('Une erreur est survenue durant la suppression de toutes les conditions');
        });
}

function removeAllRules(nodeId) {
    return $.get(Routing.generate('rule_remove_all', {nodeId: nodeId}));
}

function sendRules(form, nodeId) {
    return $.post(Routing.generate('rule_create', {nodeId: nodeId}), {form});
}