import * as rule from "node/rule";

export {edit, editTitle, viewRenderedNodeInModal, save, close, validate, addChild, addRoot, displayAccessModal, remove};

/**
 *
 * @param e
 */
function save(e) {
    e.preventDefault();
    const nodeId = $(this).data('node-id'),
        newContent = $('#node_content').val(),
        $icon = $(this).find('i');

    $icon.removeClass().addClass('fa fa-spinner fa-spin');
    saveContent(nodeId, newContent)
        .then(function () {
            $icon.removeClass().addClass('fa fa-floppy-o');
        })
        .fail(function (resp) {
            alert(resp.error);
        })
}

/**
 *
 * @param e
 */
function edit(e) {
    e.preventDefault();
    var nodeId = $(this).data('node-id');

    editContent(nodeId)
        .then(function (resp) {
            $('.box-content').html(resp);
            $('.node-edit-content').hide();
            $('.node-view-content').show();
        })
        .fail(function (resp) {
            alert(resp.error);
        })
}

/**
 *
 * @param e
 */
function editTitle(e) {
    e.preventDefault();

    var key = $(this).data('key');

    var node = $("#treetable").fancytree('getNodeByKey', key);
    node.editStart();
}

/**
 *
 * @param e
 */
function viewRenderedNodeInModal(e) {
    e.preventDefault();
    var nodeId = $(this).data('node-id');

    viewContent(nodeId)
        .then(function (resp) {
            "use strict";
            insert('<div id="render-modal" title="Field properties">' + resp + '</div>');
            $('#render-modal').dialog({
                minWidth: 1000,
                maxWidth: 1200,
                close: function (event, ui) {
                    $("#render-modal").dialog('destroy').remove();
                }
            })
        })
        .fail(function (resp) {
            alert(resp.error);
        })
}

/**
 * When we click on the add child button
 * @param e
 */
function addChild(e) {
    e.preventDefault();
    var key = $(this).data('key'),
        parentNodeId = $(this).data('id');

    addSubNode(parentNodeId)
        .then(function (resp) {
            var node = $("#treetable").fancytree('getNodeByKey', key);//.(key);
            node.setActive(false)

            node.addChildren({title: resp.title, _id: resp._id, focus: true, active: true});
            node.setExpanded(true);
        });
}

/**
 * When we click on the "ADD ROOT" button
 * @param e
 */
function addRoot(e) {
    e.preventDefault();
    var templateId = $('#template-id').val(),
        rootTitle = $('#root-title').val().trim();

    if (rootTitle.length) {
        addRootNode(templateId, rootTitle)
            .then(function (resp) {
                var $treeTable = $("#treetable"),
                    activeNode = $treeTable.fancytree('getActiveNode');
                $('#root-title').val('');
                if (activeNode) {
                    activeNode.setActive(false);
                }
                $treeTable.fancytree("getRootNode").addChildren({
                    title: resp.title,
                    _id: resp._id,
                    focus: true,
                    active: true
                });
            });
    }
}

/**
 * When we click on the "ADD ROOT" button
 * @param e
 */
function remove(e) {
    e.preventDefault();
    var key = $(this).data('key'),
        nodeId = $(this).data('id');

    if (confirm('Are you sure ?')) {
        deleteNode(nodeId)
            .then(function (resp) {
                var node = $("#treetable").fancytree('getNodeByKey', key);//.(key);
                node.remove();
            });
    }
}

/**
 *
 * @param e
 */
function close(e) {
    e.preventDefault();
    var nodeId = $(this).data('node-id');
    unlockNode(nodeId)
        .fail(function (resp) {
            alert(resp.error);
        });
    $('#ckeditor-container').html('')
}

/**
 *
 * @param e
 */
function validate(e) {
    e.preventDefault();

    const nodeId = $(this).data('id'),
        newValidation = !$(this).data('validated'), // toggle: new Value = !oldValue
        $icon = $(this).find('i');

    $icon.removeClass().addClass('fa fa-spinner fa-spin');

    nodeValidate(nodeId, newValidation)
        .then((resp) => {

            if (resp.nodeValidated) {
                $icon.removeClass().addClass('fa fa-lock');
                $(this).data('validated', 1);
            } else {
                $icon.removeClass().addClass('fa fa-unlock');
                $(this).data('validated', 0);
            }
        })
        .fail((resp) => {
            alert(resp.error);
        });
}

function displayAccessModal(e) {
    e.preventDefault();

    const templateId = $("#template-id").val(),
        nodeId = $(this).data('id');

    getNodeAccessModal(templateId, nodeId)
        .then(insert);
}

/**
 *
 * @param resp
 */
function insert(resp) {
    $('body').append(resp);
    enableModal();
    enableValidityDate();
}

/**
 *
 */
function enableModal() {
    $('#access-modal').dialog({
        modal: true,
        minWidth: 600,
        maxWidth: 600,
        buttons: {
            'Save conditions': rule.create,
            'Remove all conditions for this node': function () {
                if (confirm('Êtes vous sur ?')) {
                    rule.removeAll();
                }
            },
            Cancel: function () {
                if (confirm('Êtes vous sur ?')) {
                    $("#access-modal").dialog('destroy').remove();
                }
            }
        },
        close: function (event, ui) {
            $("#access-modal").dialog('destroy').remove();
        }
    });
    $("#tabs").tabs();
}

function enableValidityDate() {
    $("#rules-validity-from, #rules-validity-to").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: 'dd/mm/yy'
    });
}

function viewContent(nodeId) {
    return $.get(Routing.generate('node_view_content', {nodeId: nodeId}));
}

function editContent(nodeId) {
    return $.get(Routing.generate('node_edit_content', {nodeId: nodeId}));
}

function addSubNode(parentNodeId) {
    return $.get(Routing.generate('node_add_sub', {parentId: parentNodeId}));
}

function deleteNode(nodeId) {
    return $.get(Routing.generate('node_remove', {nodeId: nodeId}));
}

function addRootNode(templateId, title) {
    return $.post(Routing.generate('node_create', {parentId: templateId}), {title: title});
}

function saveContent(nodeId, content) {
    return $.post(Routing.generate('node_save_content', {nodeId: nodeId}), {content: content});
}

function unlockNode(nodeId) {
    return $.post(Routing.generate('node_stop_edition', {nodeId: nodeId}));
}

function nodeValidate(nodeId, newValidation) {
    const route = newValidation ? 'node_validate' : 'node_invalidate';
    return $.get(Routing.generate(route, {nodeId: nodeId}));
}

function getNodeAccessModal(templateId, nodeId) {
    return $.post(Routing.generate('modal_access_node', {templateId, nodeId}));
}