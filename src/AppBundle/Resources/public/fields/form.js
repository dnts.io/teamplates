import * as modal from "fields/modal";

export {listenChangeInit};

function listenChangeInit() {

    // When we check or uncheck
    $('body')
        .on('change', '#is-limited-on-time', isLimitedOnTimeChangeEvent)
        .on('change', '#field-type', fieldTypeChange);
}

/**
 *
 */
function isLimitedOnTimeChangeEvent() {
    const isChecked = $(this).is(':checked');

    $('#time-range-container')[isChecked ? 'show' : 'hide']();
    if (isChecked) {
        initDatePicker($('#time-range-to'));
        initDatePicker($('#time-range-from'));
    }
    modal.recenter();
}


function fieldTypeChange() {
    const currentField = $(this).val();

    hideAllOptions();

    switch (currentField) {
        case 'text' :
            displayTextOptions();
            break;

        case 'numeric' :
            displayNumericOptions();
            break;

        case 'age' :
            displayAgeOptions();
            break;

        case 'boolean':
            // nothing
            break;

        case 'group-subscriber' :
            //displayAndHideGroupSubscribersOption();
            break;

        case 'date' :
            displayDateOptions();
            initFieldsOptions();
            break;

        default:
            alert('This fields will be available soon');
            break;
    }

    modal.recenter();
}


function hideAllOptions() {

    $('#field-text-container').hide();
    $('#field-numeric-container').hide();
}


function displayTextOptions() {
    $('#field-text-container').show();
}

function displayNumericOptions() {
    $('#field-numeric-container').show();
}

function displayAgeOptions() {
    $('#field-age-container').show();
}

function displayDateOptions() {
    $('#field-date-container').show();
}

function initFieldsOptions() {
    initDatePicker($('#field-date-min'));
    initDatePicker($('#field-date-max'));
}


function displayAndHideGroupSubscribersOption() {

    $('#is-limited-on-time-container').hide();
    $('#field-is-required-container').hide();
    $('#field-default-value-container').hide();
}


/**
 *
 * @param $element
 * @param params
 */
function initDatePicker($element, params) {

    const defaults = {
        changeMonth: true,
        changeYear: true,
        dateFormat: 'dd/mm/yy'
    };
    const options = Object.assign(defaults, params);

    $element.datepicker(options);
}