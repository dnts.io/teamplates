import * as modal from "fields/modal";
import {listenChangeInit} from "fields/form";

export {initFields, enableAddFieldClickEvent, onOkClickEvent, onCancelClickEvent};


function initFields() {
    enableAddFieldClickEvent();
    listenChangeInit();
}

function enableAddFieldClickEvent() {
    $('body').on('click', '.node-insert-field', function (e) {
        e.preventDefault();

        modal.display();
    });
}


function onOkClickEvent() {

    // We will send the serialized form to the API and check if everything seems "ok"

    const serializedForm = $('#field-creator-form').serialize(),
        $fieldErrors = $('#field-errors'),
        templateId = $('#template-id').val(),
        isFieldExistingVisible = $('#field-existing').is(':visible');

    $fieldErrors.hide().html('');

    if (isGroupField()) {
        sendNewGroup(serializedForm, templateId)
            .then(function (resp) {
                return loadGroupFieldInfo(resp.groupId);
            })
            .then(addGroupPlaceholderIntoCkeditor)
            .then(modal.close);
    } else {

        if (isFieldExistingVisible) {
            const $existingField = $('#existing-field'),
                fieldId = $existingField.val(),
                $selectedField = $('option:selected', $existingField),
                fieldName = $selectedField.text(),
                fieldType = $selectedField.data('field-type')
                ;

            if ('group' === fieldType) {
                loadGroupFieldInfo(fieldId)
                    .then(addGroupPlaceholderIntoCkeditor);
            } else {
                addPlaceholderIntoCkeditor(fieldName, fieldId);
            }
            modal.close();
        } else {
            sendNewField(serializedForm, templateId)
                .then((resp) => {

                    if (resp.success) {
                        addPlaceholderIntoCkeditor(resp.fieldName, resp.fieldId);
                        modal.close();
                    } else {
                        $fieldErrors.html(resp.errors.join('<br/>')).show();
                    }
                });
        }
    }
}

function isGroupField() {
    return /^group\-/.test($('#field-type').val());
}

function onCancelClickEvent() {
    if (confirm('Are you sure ?')) {
        modal.close();
    }
}

function loadGroupFieldInfo(groupFieldId) {
    return $.get(Routing.generate('field_group_get_infos', {groupFieldId}));
}

function sendNewField(form, templateId) {
    return $.post(Routing.generate('field_create'), {form, templateId});
}

function sendNewGroup(form, templateId) {
    return $.post(Routing.generate('field_group_create'), {form, templateId});
}

function addGroupPlaceholderIntoCkeditor(data) {

    console.log('addGroupPlaceholderIntoCkeditor init ')
    let fieldHtml = '',
        fields = data.fields,
        groupField = data.groupField;

    Object.entries(fields).forEach(([key, field]) => {
        fieldHtml += `<span class="field-container" data-field-id="${field.id}">[[${field.name}]]</span> <br/>`;
    });

    CKEDITOR.instances['node_content'].execCommand('addblockfields', {
        startupData: {
            html: `${fieldHtml}`,
            fields,
            groupField
        }
    });
}

function addPlaceholderIntoCkeditor(fieldName, fieldId) {
    CKEDITOR.instances['node_content'].execCommand('addfield', {
        startupData: {
            name: fieldName.trim(),
            fieldId: fieldId
        }
    });
}