import {onOkClickEvent, onCancelClickEvent} from "fields/event";

export {display, close, recenter};

function display() {
    getFieldModal(getCurrentTemplateId())
        .then(insert);
}


function getCurrentTemplateId() {
    return $("#template-id").val();
}
/**
 *
 * @param resp
 */
function insert(resp) {
    $('body').append(resp);
    enableModal();
}

/**
 *
 */
function enableModal() {
    $('#field-modal').dialog({
        minWidth: 500,
        maxWidth: 500,
        buttons: {
            Ok: onOkClickEvent,
            Cancel: onCancelClickEvent
        },
        close: function (event, ui) {
            $("#field-modal").dialog('destroy').remove();
        }
    });
    $("#tabs").tabs();
}

function close() {
    $("#field-modal").dialog('destroy').remove();
}

function recenter() {
    $("#field-modal").dialog("option", "position", {my: "center", at: "center", of: window});
}

/**
 *
 * @returns {*}
 */
function getFieldModal(templateId) {
    return $.get(Routing.generate('modal_field', {templateId: templateId}));
}
