import 'select2';

export {initAttribute};


/**
 * Usefull
 */
var selectNode = null;

function initAttribute() {

    console.log('start attribute')

    $('body').on('click', '.save-template-attribute', clickSaveTemplateAttribute);
    $('.add-template-attribute').click(clickAddTemplateAttribute);
}


function clickSaveTemplateAttribute(e) {
    e.preventDefault();
    var attributeType = $('#attributetype').val();

    var $errorContainer = $('.template-attribute-error');
    $errorContainer.hide();

    saveNewTemplateAttribute(attributeType, $('#form-template-attribute').serialize())
        .then(function (resp) {
            if (resp.success) {
                prefillSelect(resp.data);
                $('#modal-dialog').remove();
            } else {
                $errorContainer.html(resp.msg).show();
            }
        });
}

/**
 * Will prefill the "select" node
 * @param data
 */
function prefillSelect(data) {
    if(selectNode) {
        selectNode.select2("trigger", "select", {data});
    }
}

function clickAddTemplateAttribute(e) {
    e.preventDefault();
    var attributeType = $(this).data('attributeType'),
        title = $(this).data('title');

    selectNode = getSelectNode($(this));

    getTemplateAttributeForm(attributeType)
        .then(function (resp) {
            createModal(title, resp);
            $('#attributetype').val(attributeType);
        });
}

/**
 * Based on the $item node, it will select the near "select" tag
 * @param $item
 * @returns {*}
 */
function getSelectNode($item) {
    return $item.parent('div').find('select.select2entity');
}


function createModal(title, content) {
    $('#modal-dialog').remove();
    $('body').append(
        `<div id="modal-dialog" title="${title}">
            <p>${content}</p>
         </div>
        `);

    $( "#modal-dialog" ).dialog({
        modal: true,
        position: { my: "center", at: "center", of: window}
    });
}


function getTemplateAttributeForm(type) {
    return $.get(Routing.generate('template_attribute_form', {type: type}));
}

function saveNewTemplateAttribute(type, serializedForm) {
    return $.post(Routing.generate('template_attribute_add', {type: type}), serializedForm);
}

