export default initSuggestion;


var filtredTemplateValue = {
    domain: {},
    type: {},
    category: {},
    keywords: []
};

function initSuggestion() {

    const $templateDomain = $('#template_domain').data('internal-type', 'domain'),
        $templateType = $('#template_type').data('internal-type', 'type'),
        $templateCategory = $('#template_category').data('internal-type', 'category'),
        $templateKeywords = $('#template_keywords').data('internal-type', 'keywords');

    [$templateDomain, $templateType, $templateCategory, $templateKeywords].forEach(($selector) => {

        // Calculate the last selected values
        const selectValue = $selector.val();
        if (selectValue) {
            const attributeType = $selector.data('internal-type'),
                data = {id: $selector.val()};
            addAttributesToFilter(attributeType, data);
        }

        // Add listener when we select a value
        $selector.on('select2:select', selectAttributeEvent);
    });

    searchItems(filtredTemplateValue)
        .then(renderSuggestion);

    $templateKeywords.on('select2:unselect', removeItemToFilter('keyword'));
}


function selectAttributeEvent(e) {

    const attributeType = $(this).data('internal-type'),
        data = formatData(e.params.data);

    addAttributesToFilter(attributeType, data);

    searchItems(filtredTemplateValue)
        .then(renderSuggestion);
}

function addAttributesToFilter(attributeType, data) {
    if (attributeType === 'keyword') {
        filtredTemplateValue[attributeType].push(data);
    } else {
        filtredTemplateValue[attributeType] = data;
    }
}

function removeItemToFilter(attributeType) {

    return function (e) {
        var data = formatData(e.params.data);

        // @TODO: remove item when unselect it

        console.log(filtredTemplateValue);
    }
}

function formatData(data) {
    delete data.selected;
    delete data.text;

    return data;
}

function renderSuggestion(resp) {
    $('#template-list-suggestion').html(resp);
}

function searchItems(filters) {
    return $.post(Routing.generate('template_search'), {filters})
}