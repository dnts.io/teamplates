export {init, loadNodePreview, loadNodePreviewAndHandleFields, showFields};

function init() {
    const $currentNode = $('#current-node-id');
    if ($currentNode.length) {
        loadNodePreviewAndHandleFields($currentNode.val());
    }
}

function loadNodePreviewAndHandleFields(nodeId) {

    hideAllFields();

    return loadNodePreview(nodeId)
        .then(showFields)
        .then(showTable)
}

function loadNodePreview(nodeId) {

    const fillingId = $('#filling-id').val();

    return getNodeContent(fillingId, nodeId)
        .then(insert)
}

function insert(resp) {
    $('.preview-holder').html(resp)
}

function showTable() {
    $('#template-fill-form').find('table').show();
}

function hideAllFields() {
    $(`.group-field-table`).find('tr').hide();
    $(`.fillable-input`).parents('tr').hide();
    $(`.checkable-input`).parents('tr').hide();
}

function showFields() {
    let nbOfField = 0;
    $.each($('.editor-placeholder'), function (i, item) {
        const fieldId = $(item).data('fieldId');
        $(`.fillable-input[data-field-id='${fieldId}']`).parents('tr').show();
        $(`.checkable-input[data-field-id='${fieldId}']`).parents('tr').show();

        nbOfField++;
    });

    $.each($('.field-block'), function (i, item) {
        const fieldId = $(item).data('fieldId');
        $(`.group-${fieldId}`).show();

        nbOfField++;
    });


    return nbOfField;
}


function getNodeContent(fillingId, nodeId) {
    return $.get(Routing.generate('filling_view_only_node', {fillingId, nodeId}));
}