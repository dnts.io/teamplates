import * as filling from "filling/filling";
import * as preview from "filling/preview";
import * as progressBar from "filling/progress-bar";

export {initFilling};

function initFilling() {

    preview.init();
    progressBar.updateMain();
    $('#filling-save').click(filling.saveEvent);

    initDateField();
    changePageHandleKeyPress();
    pageNavigateHandle();
    fieldChangeKeyPressHandle();
    fieldCheckHandle();
    addGroupItemClickHandle();
    fieldBlurHandle();
}

function initDateField() {
    $('[data-field-type="date"]').datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: 'dd/mm/yy',
        onSelect: function() {
            $(this).blur().change();
        }
    });
}


function fieldBlurHandle() {
    $('#template-fill-form').on('blur', '.fillable-input', function () {

        let fieldId = $(this).data('field-id'),
            value = $(this).val();

        if (/_/.test(fieldId)) {
            fieldId = fieldId.split('_')[0];
        }

        filling
            .checkField(fieldId, value)
            .then((resp) => {

                const $errorBox = $(this).parents('td').children('.errors-fields');
                if (resp.valid) {
                    $errorBox.hide();
                } else {
                    $errorBox.show().html(resp.errors.join('<br/>'));
                }
            })
    });
}

/**
 * This function will handle when we want to add a item to field group
 */
function addGroupItemClickHandle() {
    $('.add-new-element-to-group ').click(function (e) {
        e.preventDefault();

        const groupId = $(this).data('groupId'),
            fillingId = $('#filling-id').val(),
            currentPage = $('.pager').data('current-page'),
            nodes = getNodes();

        getNewGroupItem(groupId, fillingId)
            .then(function (resp) {
                let $lastItem = $(`.field-${groupId}:last`);

                if (!$lastItem.length) {
                    $lastItem = $(`.group-${groupId}`);
                }

                $lastItem.after(resp.item);
                preview
                    .loadNodePreview(nodes[currentPage - 1])
                    .then(preview.showFields)
            })
    });
}


function pageNavigateHandle() {
    $('.pager-next').on('click', function (e) {
        e.preventDefault();
        if (!$(this).hasClass('disabled')) {
            const currentPage = $('.pager').data('current-page'),
                nextPage = currentPage + 1,
                nodes = getNodes(),
                nbOfNodes = nodes.length;

            $('.pager-next').addClass('disabled');

            if (nextPage <= nbOfNodes) {
                $('.pager').data('current-page', nextPage);
                filling
                    .save()
                    .then(progressBar.updateMain)
                    .then(preview.loadNodePreviewAndHandleFields(nodes[nextPage - 1]))
                    .then(() => {
                        $('.pager-next').removeClass('disabled');
                        $('#switch-to-page').val(nextPage);
                        $('.pager-previous').removeClass('disabled');
                        $('.pager-next')[(nextPage === nbOfNodes) ? 'addClass' : 'removeClass']('disabled');
                    });
            }
        }
    });


    $('.pager-previous').on('click', function (e) {
        e.preventDefault();
        if (!$(this).hasClass('disabled')) {
            const currentPage = $('.pager').data('current-page'),
                previousPage = currentPage - 1,
                nodes = getNodes();

            $('.pager-previous').addClass('disabled');

            if (previousPage >= 0) {
                $('.pager').data('current-page', previousPage);
                filling
                    .save()
                    .then(progressBar.updateMain)
                    .then(preview.loadNodePreviewAndHandleFields(nodes[previousPage - 1]))
                    .then(() => {
                        $('.pager-previous').removeClass('disabled');
                        $('#switch-to-page').val(previousPage);
                        $('.pager-next').removeClass('disabled');
                        $('.pager-previous')[(previousPage === 1) ? 'addClass' : 'removeClass']('disabled');
                    });
            }
        }
    });
}

function changePageHandleKeyPress() {

    $('#switch-to-page').on('keypress', function (e) {
        if (e.which === 13) {
            const nodeIds = getNodes();
            let pageId = 0;

            if ($(this).val() <= nodeIds.length && $(this).val() > 0) {
                pageId = +$(this).val();
            } else {
                pageId = 1;
                $(this).val(1);
            }

            $('.pager').data('current-page', pageId);
            $('#filling-save').click();
            filling
                .save()
                .then(progressBar.updateMain)
                .then(preview.loadNodePreviewAndHandleFields(nodeIds[pageId - 1]));
        }
    });
}

function getNodes() {
    return JSON.parse($('#node-ids').val());
}

function fieldCheckHandle() {

    $('#template-fill-form').on('change', '.checkable-input', function () {
        const $this = $(this),
            fieldId = $this.data('fieldId'),
            isChecked = $this.is(':checked'),
            $previewItem = $(`.editor-placeholder[data-field-id="${fieldId}"]`);
            //previousPlaceHolder = $previewItem.data('previous-placeholder'); // in case we have it


        if (isChecked) {
            $previewItem.text('yes');
        } else {
            $previewItem.text('no');
        }
    });
}

function fieldChangeKeyPressHandle() {

    $('#template-fill-form').on('change', '.fillable-input', function () {
        const $this = $(this),
            fieldId = $this.data('fieldId'),
            value = $this.val(),
            $previewItem = $(`.editor-placeholder[data-field-id="${fieldId}"]`),
            previousPlaceHolder = $previewItem.data('previous-placeholder'); // in case we have it

        // Special case
        if (value.length === 1) {
            const placeholder = $previewItem.text();
            $previewItem.attr('data-previous-placeholder', placeholder);
        }

        if (value.length > 0) {
            $previewItem.text(value);
        } else {
            $previewItem.text(previousPlaceHolder);
        }
    });
}


function getNewGroupItem(groupFieldId, fillingId) {
    return $.get(Routing.generate('field_new_group_item', {groupFieldId, fillingId}));
}