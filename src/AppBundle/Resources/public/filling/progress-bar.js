export {updateMain};

function updateMain() {

    const fillingId = $('#filling-id').val();

    if(!fillingId) {
        return;
    }

    return getDocumentStats(fillingId)
        .then((resp) => {
            $('.progress-bar').width(resp.percent + '%');
        })
}

function getDocumentStats(fillingId) {
    return $.get(Routing.generate('filling_stats', {fillingId}));
}