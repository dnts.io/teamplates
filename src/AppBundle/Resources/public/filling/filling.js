import * as progressBar from "filling/progress-bar";

export {save, saveEvent, checkField};

function saveEvent(e) {

    e.preventDefault();

    const $icon = $(this).find('i');
    $icon.removeClass().addClass('fa fa-spinner fa-spin');

    save()
        .then(progressBar.updateMain)
        .then(() => {
            $icon.removeClass().addClass('fa fa-floppy-o');
        });
}

function save() {
    const formSerialized = $('#template-fill-form').serialize(),
        fillingId = $('#filling-id').val();

    return saveFormFilling(formSerialized, fillingId);
}


function saveFormFilling(form, fillingId) {
    return $.post(Routing.generate('filling_save', {fillingId}), {form});
}

function checkField(fieldId, value) {
    return $.post(Routing.generate('field_validate', {fieldId}), {value});
}