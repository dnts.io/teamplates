(function (CKEDITOR) {
    'use strict';

    CKEDITOR.plugins.add('addblockfields', {
        requires: 'widget',

        init: function (editor) {
            editor.widgets.add('addblockfields', {

                template: '<div class="field-block"></div>',

                editables: {
                    content: {
                        selector: '.field-block',
                        allowedContent: 'p br ul ol li strong em span'
                    }
                },

                allowedContent: 'div(!field-block)',

                requiredContent: 'div(field-block)',

                upcast: function (element) {
                    return element.name == 'div' && element.hasClass('field-block');
                },

                data: function () {
                    if (this.data.html) {

                        this.element.setHtml(this.data.html);
                        this.element.data('field-id', this.data.groupField.id);

                        var nodes = this.element.find('span.field-container');
                        if (nodes.count()) {

                            var fields = this.data.fields;

                            for (var i = 0, len = nodes.count(); i < len; i++) {
                                var currentNode = nodes.getItem(i),
                                    cleanedNodeText = currentNode.getText().replace('[[', '').replace(']]', ''),
                                    currentField = fields[cleanedNodeText];

                                editor.widgets.initOn(currentNode, 'addfield', {fieldId: currentField.id});
                            }
                        }
                    }
                },
            });
        }
    });

})(CKEDITOR);
