(function (CKEDITOR) {
    'use strict';

    CKEDITOR.plugins.add('addfield', {
        requires: 'widget,dialog',

        init: function (editor) {

            // Put ur init code here.
            editor.widgets.add('addfield', {

                template: '<span class="field-container">[[]]</span>',

                // This is called when you have to parse your data from CKEDITOR to TEXT (into rendered content)
                downcast: function () {
                    return new CKEDITOR.htmlParser.text(`<span class="editor-placeholder" data-field-id="${this.data.fieldId }">[[${this.data.name }]]</span>`);
                },

                init: function () {
                    // Note that placeholder markup characters are stripped for the name.
                    this.setData('name', this.element.getText().slice(2, -2));
                },

                data: function () {
                    this.element.setText('[[' + this.data.name + ']]');
                },

                getLabel: function () {
                    return this.editor.lang.widget.label.replace(/%1/, this.data.name + ' ' + this.pathName);
                }
            });
        },

        afterInit: function (editor) {
            var placeholderReplaceRegex = /\[\[([^\[\]])+\]\]/g;

            editor.dataProcessor.dataFilter.addRules({
                text: function (text, node) {
                    var dtd = node.parent && CKEDITOR.dtd[node.parent.name];

                    // Skip the case when placeholder is in elements like <title> or <textarea>
                    // but upcast placeholder in custom elements (no DTD).
                    if (dtd && !dtd.span)
                        return;

                    return text.replace(placeholderReplaceRegex, function (match) {
                        // Creating widget code.
                        var widgetWrapper = null,
                            innerElement = new CKEDITOR.htmlParser.element('span', {
                                'class': 'field-container'
                            });

                        // Adds placeholder identifier as innertext.
                        innerElement.add(new CKEDITOR.htmlParser.text(match));
                        widgetWrapper = editor.widgets.wrapElement(innerElement, 'addfield');

                        // Return outerhtml of widget wrapper so it will be placed
                        // as replacement.
                        return widgetWrapper.getOuterHtml();
                    });
                }
            });
        },
        onLoad: function () {
            CKEDITOR.addCss('.field-container { background: yellow }');
        }
    });


})(CKEDITOR);
