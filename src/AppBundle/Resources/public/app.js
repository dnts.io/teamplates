import {initTree} from "node/tree";
import {initAttribute} from "template-attribute/attribute";
import initSuggestion from "template-attribute/suggestions";
import {initFields} from "fields/event";
import {expandHandle} from "template/template";
import {initFilling} from "filling/events";
import {areYouSureHandler} from "helpers";
import {initDashboard} from "dashboard/menu";

export {init};

function init() {
    console.log('start app');
    initTree();
    initAttribute();
    initSuggestion();
    initFields();
    expandHandle();
    initFilling();

    initDashboard();

    areYouSureHandler();
}

