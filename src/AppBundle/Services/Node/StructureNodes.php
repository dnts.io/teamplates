<?php

namespace AppBundle\Services\Node;

use AppBundle\Document\Node;
use Gedmo\Tree\Document\MongoDB\Repository\MaterializedPathRepository;

class StructureNodes
{
    /** @var MaterializedPathRepository */
    protected $nodeRepository;

    /** @var string[] */
    protected $flattenNodes;

    /**
     * NodeHtmlRender constructor.
     *
     * @param MaterializedPathRepository $nodeRepository
     */
    public function __construct(MaterializedPathRepository $nodeRepository)
    {
        $this->nodeRepository = $nodeRepository;
    }

    /**
     * @param Node $node
     *
     * @return string[]
     */
    public function getFlattenNodes(Node $node)
    {
        $this->flattenNodes = [];

        $items = $this->nodeRepository->childrenHierarchy($node, false, [], true);
        $this->getFlattenSubNodes(array_pop($items));

        return $this->flattenNodes;
    }

    /**
     * @param array $node
     */
    protected function getFlattenSubNodes(array $node)
    {
        $this->flattenNodes[] = $this->getNodeIdAsString($node);

        foreach ($node['__children'] as $child) {
            $this->getFlattenSubNodes($child);
        }
    }

    protected function getNodeIdAsString(array $node)
    {
        /** @var \MongoId $nodeObjectId */
        $nodeObjectId = $node['_id'];

        return (string) $nodeObjectId->toBSONType();
    }

    public function getTreeNodes(Node $node)
    {
        $items = $this->nodeRepository->childrenHierarchy($node, false, [], true);

        $content = $this->getTreeSubNodes(array_pop($items));

        return $content;
    }


    /**
     * @param array $node
     *
     * @return string
     */
    protected function getTreeSubNodes(array $node)
    {
        $treeNode = [
            'id'    => $this->getNodeIdAsString($node),
            'title' => isset($node['title']) ? $node['title'] : '-',
            'path'  => $node['path'],
            'level'  => $node['level'],
        ];

        foreach ($node['__children'] as $child) {
            $treeNode['children'][] = $this->getTreeSubNodes($child);
        }

        return $treeNode;
    }
}
