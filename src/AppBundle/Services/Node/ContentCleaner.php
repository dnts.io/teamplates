<?php

namespace AppBundle\Services\Node;

class ContentCleaner
{
    protected $content;

    const CLEANING_NB_PASS = 5;

    public function __construct()
    {

    }

    public function clean($content)
    {
        $this->content = $content;

        $this->cleanImbricateFieldHolder();

        return $this->content;
    }


    /**
     * Sometime stuff like this can happend:
     * <span class="editor-placeholder" data-field-id="57dbbec5f7a4c040673ccbd7">
     *      <span class="editor-placeholder" data-field-id="undefined">[[Nom Companie d'Assurance]]</span>
     * </span>
     *
     * This part is too much : <span class="editor-placeholder" data-field-id="undefined">(ok content)</span>
     */
    protected function cleanImbricateFieldHolder()
    {
        // Sometime it can be more than one "<span class="editor-placeholder" data-field-id="undefined"></span>"
        for ($i = 0; $i < self::CLEANING_NB_PASS; $i++) {
            $pattern       = '#<span class="editor-placeholder" data-field-id="undefined">(.+)</span>#U';
            $replacement   = '$1';
            $this->content = preg_replace($pattern, $replacement, $this->content);
        }
    }
}
