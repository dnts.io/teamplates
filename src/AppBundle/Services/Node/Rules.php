<?php

namespace AppBundle\Services\Node;

use AppBundle\Services\Field\Form;

/**
 * This class will convert a form of rules into an array of rules
 *
 */
class Rules
{
    /**
     * @var Form
     */
    protected $form;

    public function __construct(Form $form)
    {
        $this->form = $form;
    }

    /**
     * @return array
     */
    public function getRules()
    {
        $rules = [];

        foreach ($this->form->get('field') as $key => $field) {
            $rules[$key]['field'] = $field;
        }

        foreach ($this->form->get('field-logic') as $key => $fieldLogic) {
            $rules[$key]['field-logic'] = $fieldLogic;
        }

        foreach ($this->form->get('field-condition') as $key => $fieldCondition) {
            $rules[$key]['field-condition'] = $fieldCondition;
        }

        foreach ($this->form->get('field-value-condition') as $key => $fieldValueCondition) {
            $rules[$key]['field-value-condition'] = $fieldValueCondition;
        }

        return $rules;
    }

    /**
     * @return mixed|null
     */
    public function getRulesAction()
    {
        return $this->form->get('field-action');
    }

    /**
     * @return array
     */
    public function getRulesValidity()
    {
        if (!$this->form->get('rules-validity-from')) {
            return null;
        }

        return [
            'from' => $this->form->get('rules-validity-from'),
            'to'   => $this->form->get('rules-validity-to'),
        ];
    }
}
