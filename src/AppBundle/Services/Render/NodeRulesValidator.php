<?php

namespace AppBundle\Services\Render;

use AppBundle\Document\Field;
use AppBundle\Document\Filling;
use AppBundle\Document\Node;
use AppBundle\Document\NodeRule;
use Doctrine\Common\Persistence\ObjectRepository;

class NodeRulesValidator
{

    protected $nodeArray;

    protected $nodeObject;

    protected $mode;

    /**
     * @var ObjectRepository
     */
    protected $repository;

    public function __construct(ObjectRepository $repository)
    {
        $this->repository = $repository;
    }


    public function isDisplayable($node, Filling $filling)
    {
        // Don't need to retrive the node in this case
        if (empty($node['rules'])) {
            return true;
        }

        // TODO REFACTOR THIS

        $this->nodeArray = $node;

        $fillerValue = $filling->getValues();

        $returnBooleanValue = true;

        /** @var NodeRule $rule */
        foreach ($this->getNode()->getRules() as $rule) {

            /** @var Field $field */
            $field = $rule->getField();
            $bool  = true;

            if (isset($fillerValue[$field->getId()])) {

                $value = $fillerValue[$field->getId()];

                if ($rule->getRule() === 'eq') {
                    $bool = $value === $rule->getValue();
                }

                if ($rule->getRule() === 'noteq') {
                    $bool = $value !== $rule->getValue();
                }
            }

            if ($rule->getLogic() === 'and') {
                $returnBooleanValue = $returnBooleanValue && $bool;
            } else {
                $returnBooleanValue = $returnBooleanValue || $bool;
            }
        }

        return $returnBooleanValue;
    }

    /**
     * @return Node
     */
    private function getNode() : Node
    {
        /** @var Node $node */
        $node = $this->repository->find($this->nodeArray['_id']);

        return $node;
    }
}
