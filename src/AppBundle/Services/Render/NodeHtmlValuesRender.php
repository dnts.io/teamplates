<?php

namespace AppBundle\Services\Render;

use AppBundle\Document\Filling;
use AppBundle\Document\Node;
use Gedmo\Tree\Document\MongoDB\Repository\MaterializedPathRepository;
use Gedmo\Tree\RepositoryInterface;
use Symfony\Component\HttpFoundation\Response;

class NodeHtmlValuesRender
{
    /**
     * @var MaterializedPathRepository
     */
    protected $nodeRepository;

    /**
     * @var NodeRulesValidator
     */
    protected $nodeRulesValidator;

    public function __construct(RepositoryInterface $nodeRepository, NodeRulesValidator $nodeRulesValidator)
    {
        $this->nodeRepository     = $nodeRepository;
        $this->nodeRulesValidator = $nodeRulesValidator;
    }

    /**
     * @param Node    $node
     * @param Filling $filling
     * @param bool    $withChild
     *
     * @return Response
     */
    public function getNodeRenderResponse(Node $node, Filling $filling, $withChild = true)
    {
        if ($withChild) {
            $items   = $this->nodeRepository->childrenHierarchy($node, false, [], true);
            $content = $this->getNodeContent(array_pop($items), $filling, $withChild);
        } else {
            $arrayNode = [
                '_id'     => $node->getId(),
                'title'   => $node->getTitle(),
                'content' => $node->getContent(),
                'level'   => $node->getLevel(),
            ];
            $content   = $this->getNodeContent($arrayNode, $filling, $withChild);
        }

        return new Response($content);
    }

    /**
     * @param array   $node
     * @param Filling $filling
     * @param bool    $withChild
     *
     * @return string
     */
    protected function getNodeContent(array $node, Filling $filling, $withChild)
    {
        $content = '';
        if ($this->nodeRulesValidator->isDisplayable($node, $filling)) {

            $content .= isset($node['title']) ? $this->generateTitle($node['title'], $node['level']) : '';
            $content .= (string) isset($node['content']) ? $this->parseContent($node['content'], $filling) : '';

            if (isset($node['__children'])) {
                foreach ($node['__children'] as $child) {
                    $content .= $this->getNodeContent($child, $filling, $withChild);
                }
            }
        }

        return $content;
    }

    /**
     * @param string $title
     * @param int    $level
     *
     * @return string
     */
    protected function generateTitle($title, $level)
    {
        $hLevel = 'h'.($level - 1);

        return '<'.$hLevel.'>'.$title.'</'.$hLevel.'>';
    }

    protected function parseContent($content, Filling $filling)
    {
        $values = $filling->getValues();

        // Render simple field
        if ($values) {
            foreach ($values as $fieldId => $value) {
                $content = $this->parseFields($content, $fieldId, $value);
            }
        }

        $groupFields = $filling->getGroupValue();
        // Render grouped field
        if ($groupFields) {
            foreach ($groupFields as $groupFieldId => $items) {
                $content = $this->parseGroup($content, $items, $groupFieldId);
            }
        } else {
            $content = $this->parseGroup($content);
        }

        return $content;
    }


    /**
     * @param $content
     * @param $fieldId
     * @param $value
     *
     * @return mixed
     */
    protected function parseFields($content, $fieldId, $value)
    {
        if ($value) {
            $acceptedChars = '[\'’–"\/_\-\.a-zA-Z0-9ÀÁÂÃÄÅÇÑñÇçÈÉÊËÌÍÎÏÒÓÔÕÖØÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöøùúûüýÿ ]+';
            $pattern       = '#<span class="editor-placeholder" data-field-id="'.$fieldId.'">(\[\['.$acceptedChars.'\]\])</span>#';
            $replacement   = '<span class="editor-placeholder" data-previous-placeholder="$1" data-field-id="'.$fieldId.'">'.$value.'</span>';
            $content       = preg_replace($pattern, $replacement, $content);
        }

        return $content;
    }

    /**
     * @param      $content
     * @param null $items
     * @param null $groupId
     *
     * @return mixed
     */
    protected function parseGroup($content, $items = null, $groupId = null)
    {
        $pattern = '#<div class="field-block" data-field-id="'.$groupId.'">((.|\s)+)</div>#iU';
        preg_match($pattern, $content, $matches);

        if ($items) {

            if (isset($matches[1])) {
                $originalGroupContent = $matches[1];
                $groupNewContent      = '';

                foreach ($items as $itemNb => $fields) {

                    $contentToReplicate = $matches[1]; // capture the "html" of the group
                    foreach ($fields as $field) {

                        // - We will apply here some transformation of our "captured html" -
                        // Each field will be transformed to be replaceable via JavaScript

                        $fieldId = $field['id'];
                        // We have to know the previous name of the field to be able to do the replace
                        $fieldName = preg_replace('/\_'.$itemNb.'$/', '', $field['name']);

                        $value = !empty($field['value']) ? $field['value'] : '[['.$field['name'].']]';

                        $pattern     = '#<span class="editor-placeholder" data-field-id="'.$fieldId.'">(\[\['.$fieldName.'\]\])</span>#';
                        $replacement = '<span class="editor-placeholder" data-previous-placeholder="$1" data-field-id="'.$fieldId.'_'.$itemNb.'">'.$value.'</span>';

                        // This part will convert field into value
                        $contentToReplicate = preg_replace($pattern, $replacement, $contentToReplicate);
                    }
                    $groupNewContent .= $contentToReplicate;
                }

                $content = str_replace($originalGroupContent, $groupNewContent, $content);
                $content = str_replace(
                    '<div class="field-block" data-field-id="'.$groupId.'">',
                    '<div data-parsed="true" class="field-block" data-field-id="'.$groupId.'">',
                    $content
                );
            }
        } else {
            $pattern     = '#<div class="field-block" data-field-id="([a-z0-9]+)">(?:(.|\s)+)</div>#iU';
            $replacement = '<div class="field-block" data-field-id="$1">GROUP FIELD</div>';
            $content     = preg_replace($pattern, $replacement, $content);
        }

        return $content;
    }
}
