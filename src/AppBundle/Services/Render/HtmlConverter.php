<?php

namespace AppBundle\Services\Render;

use Pandoc\Pandoc;
use PrinceXMLPhp\PrinceWrapper;
use pxCore\LibreOfficeConverterBundle\Services\LibreOfficeConverterService;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class HtmlConverter
{
    const FORMAT_PDF = 'pdf';
    const FORMAT_DOCX = 'docx';
    const FORMAT_ODT = 'odt';
    const FORMAT_HTML = 'html';
    const FORMAT_RTF = 'rtf';

    private $mimeTypes = [
        self::FORMAT_DOCX => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        self::FORMAT_PDF  => 'application/pdf',
        self::FORMAT_ODT  => 'application/vnd.oasis.opendocument.text',
        self::FORMAT_RTF  => 'application/rtf',
    ];

    // http://www.commandlinefu.com/commands/view/11692/commandline-document-conversion-with-libreoffice
    private $filters = [
        self::FORMAT_DOCX => 'docx:"Office Open XML Text"',
        self::FORMAT_PDF  => 'pdf:writer_pdf_Export',
        self::FORMAT_ODT  => 'odt:"OpenDocument Text Flat XML"',
        self::FORMAT_RTF  => 'rtf:"Rich Text Format"',
    ];

    /** @var LibreOfficeConverterService */
    private $libreOfficeConverterService;

    public function __construct($libreOfficeConverterService)
    {
        $this->libreOfficeConverterService = $libreOfficeConverterService;
    }

    /**
     * @param string $html
     * @param string $destinationFormat
     * @param string $outFileName
     *
     * @return BinaryFileResponse|Response
     * @throws \Exception
     */
    public function renderAsResponse($html, $destinationFormat, $outFileName)
    {
        switch (strtolower($destinationFormat)) {
            case self::FORMAT_HTML:
                return new Response($html);
            default:
                $outFile = $this->render($html, $destinationFormat);
                break;
        }

        if (!$outFile) {
            throw new \Exception('The printable document is empty');
        }

        $response = new BinaryFileResponse($outFile);
        $response->deleteFileAfterSend(true);

        $response->headers->set('Content-Type', $this->mimeTypes[$destinationFormat]);

        $response->headers->set(
            'Content-Disposition',
            $response->headers->makeDisposition(
                ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                $outFileName
            )
        );

        return $response;
    }

    /**
     * @param string $html
     * @param string $destinationFormat
     *
     * @return string
     */
    protected function render($html, $destinationFormat)
    {
        $htmlCarapace = <<<HTML
<html>
    <head>
        <style>
h1, h2, h3, h4, h5, h6 {
    page-break-before: always;
}
h1.fake-title {
    display: none;
}
table {
    width: 100%%;
}
        </style>
    </head>
    <body>
        %s
        <h1 class="fake-title">&nbsp;</h1>
    </body>
</html>
HTML;

        $basePath        = tempnam('/tmp/', 'libreOffice');
        $originPath      = $basePath.'.html';
        $destinationPath = $basePath.'.'.$destinationFormat;
        unlink($basePath);

        file_put_contents($originPath, sprintf($htmlCarapace, $html));

        $this->libreOfficeConverterService->convert(
            $originPath,
            '/tmp',
            $this->filters[$destinationFormat]
        );

        unlink($originPath);

        return $destinationPath;
    }
}
