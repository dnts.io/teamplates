<?php

namespace AppBundle\Services\Render;

use AppBundle\Document\Node;
use Gedmo\Tree\Document\MongoDB\Repository\MaterializedPathRepository;
use Symfony\Component\HttpFoundation\Response;

class NodeHtmlRender
{
    /**
     * @var MaterializedPathRepository
     */
    protected $nodeRepository;

    /**
     * NodeHtmlRender constructor.
     *
     * @param MaterializedPathRepository $nodeRepository
     */
    public function __construct(MaterializedPathRepository $nodeRepository)
    {
        $this->nodeRepository = $nodeRepository;
    }

    /**
     * @param Node $node
     *
     * @return Response
     */
    public function getNodeRenderResponse(Node $node)
    {
        $items = $this->nodeRepository->childrenHierarchy($node, false, [], true);

        $content = $this->getNodeContent(array_pop($items));

        $response = new Response();
        $response->setContent($content);

        return $response;
    }

    /**
     * @param array $node
     *
     * @return string
     */
    protected function getNodeContent(array $node)
    {
        $content = isset($node['title']) ? $this->generateTitle($node['title'], $node['level']) : '';
        $content .= (string) isset($node['content']) ? $node['content'] : '';

        foreach ($node['__children'] as $child) {
            $content .= $this->getNodeContent($child);
        }
        
        return $content;
    }

    /**
     * @param string $title
     * @param int    $level
     *
     * @return string
     */
    protected function generateTitle($title, $level)
    {
        $hLevel = 'h'.($level - 1);

        return '<'.$hLevel.'>'.$title.'</'.$hLevel.'>';
    }
}
