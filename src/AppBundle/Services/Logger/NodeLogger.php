<?php

namespace AppBundle\Services\Logger;

use AppBundle\Document\Node;

class NodeLogger extends BaseLogger
{

    public function logInfo($nodeId, $action, $state, Node $node = null)
    {
        $this->log(
            $action,
            $state,
            [
                'node_id' => \MongoDBRef::create('Node', new \MongoId($nodeId)),
                'data'    => $this->getData($node),
            ]
        );

    }

    private function getData(Node $node = null)
    {
        if (!$node) {
            return [];
        }

        return [
            'id'      => \MongoDBRef::create('Node', new \MongoId($node->getId())),
            'title'   => $node->getTitle(),
            'content' => $node->getContent(),
            'level'   => $node->getLevel(),
            'path'    => $node->getPath(),
            'parent'  => $node->getParent(),
        ];
    }
}
