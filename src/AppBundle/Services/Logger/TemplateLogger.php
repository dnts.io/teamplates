<?php

namespace AppBundle\Services\Logger;

use AppBundle\Document\Template;

class TemplateLogger extends BaseLogger
{
    public function logInfo($templateId, $action, $state, Template $template = null)
    {
        $this->log(
            $action,
            $state,
            [
                'template_id' => \MongoDBRef::create('Template', new \MongoId($templateId)),
                'data'        => $this->getData($template),
            ]
        );
    }

    private function getData(Template $template = null)
    {
        if (!$template) {
            return [];
        }

        return [
            'id'          => \MongoDBRef::create('Template', new \MongoId($template->getId())),
            'name'        => $template->getName(),
            'owner'       => $template->getOwner(),
            'description' => $template->getShortDescription(),
            'status'      => $template->getStatus(),
            'version'     => $template->getVersion(),
            'alpha_node'  => $template->getAlphaNode(),
        ];
    }
}
