<?php

namespace AppBundle\Services\Logger;

use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

abstract class BaseLogger
{

    /** @var Logger */
    protected $logger;

    /** @var RequestStack */
    protected $requestStack;

    /** @var TokenStorage */
    protected $tokenStorage;

    public function setLogger(Logger $logger)
    {
        $this->logger = $logger;
    }

    public function setRequestStack(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    public function setTokenStorage(TokenStorage $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @return string[]
     */
    protected function getClientIps()
    {
        return $this->requestStack->getCurrentRequest()->getClientIps();
    }

    /**
     * @return array|null
     */
    protected function getUserId()
    {
        if (!$this->tokenStorage->getToken()) {
            return null;
        }

        return \MongoDBRef::create('User', new \MongoId($this->tokenStorage->getToken()->getUser()->getId()));
    }

    /**
     * @param string $action
     * @param string $state
     * @param array  $info
     */
    public function log($action, $state, array $info)
    {
        $info = array_merge(
            [
                'action'   => $action,
                'state'    => $state,
                'user'     => $this->getUserId(),
                'user_ips' => $this->getClientIps(),
            ],
            $info
        );
        $this->logger->info($action.': '.$state, $info);

    }
}
