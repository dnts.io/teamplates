<?php

namespace AppBundle\Services\File;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class Upload
{
    /** @var string */
    private $schemeAndHttpHost;

    /** @var string */
    private $webDir;

    /** @var array */
    private $fileInfo = [];

    public function __construct($schemeAndHttpHost, $webDir)
    {
        $this->schemeAndHttpHost = $schemeAndHttpHost;
        $this->webDir            = realpath($webDir);
    }

    /**
     * @param UploadedFile $uploadedFile
     *
     * @return string
     */
    public function getUrl(UploadedFile $uploadedFile)
    {
        return
            $this->schemeAndHttpHost
            .$this->getPrefixPath($uploadedFile)
            .rawurlencode($uploadedFile->getClientOriginalName());
    }

    /**
     * @param UploadedFile $uploadedFile
     *
     * @return string
     */
    public function getPath(UploadedFile $uploadedFile)
    {
        return $this->webDir.$this->getPrefixPath($uploadedFile).$uploadedFile->getClientOriginalName();
    }

    /**
     * @param UploadedFile $uploadedFile
     *
     * @return string
     */
    protected function getPrefixPath(UploadedFile $uploadedFile)
    {
        return '/uploads/'.$this->getHash($uploadedFile).'-';
    }

    /**
     * @param UploadedFile $uploadedFile
     *
     * @return string
     */
    protected function getHash(UploadedFile $uploadedFile)
    {
        if (!isset($this->fileInfo[$uploadedFile->getPathname()]['hash'])) {
            $this->fileInfo[$uploadedFile->getPathname()]['hash'] = md5($uploadedFile->openFile()->fgets());
        }

        return $this->fileInfo[$uploadedFile->getPathname()]['hash'];
    }
}
