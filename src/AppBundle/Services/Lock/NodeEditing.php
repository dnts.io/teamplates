<?php

/*
 * The constructor needs the current user and the current datetime to compare them with the existent lock.
 */
namespace AppBundle\Services\Lock;

use AppBundle\Document\User;
use Doctrine\ODM\MongoDB\DocumentManager;

class NodeEditing
{
    const EXPIRATION_DURATION = 'PT30M';

    /** @var DocumentManager */
    protected $dm;

    /** @var User */
    protected $currentUser;
    /** @var \DateTime */
    protected $currentDate;

    /** @var User */
    protected $user;
    /** @var \DateTime */
    protected $startDate;

    public function __construct(User $currentUser, $currentDate, DocumentManager $dm)
    {
        $this->currentUser = $this->user = $currentUser;
        $this->currentDate = $this->startDate = new \DateTime(); // @todo take the $currentDate
        $this->dm          = $dm;
    }

    /**
     * @param User      $user
     * @param \DateTime $startDate
     */
    public function setUserAndDate(User $user, \DateTime $startDate)
    {
        $this->user      = $user;
        $this->startDate = $startDate;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return \DateTime
     */
    public function getStartDate(): \DateTime
    {
        return $this->startDate;
    }

    public function toMongoDB()
    {
        return [
            'user'  => $this->user->getId(),
            'start' => $this->startDate->format(\DateTime::W3C),
        ];
    }

    /**
     * @param array $lockData
     *
     * @return self
     */
    public function setFromMongoDB(array $lockData)
    {
        $user  = isset($lockData['user']) ? $this->dm->getRepository('AppBundle:User')->find($lockData['user']) : null;
        $start = isset($lockData['start']) ? new \DateTime($lockData['start']) : null;

        if ($user && $start) {
            $this->setUserAndDate($user, $start);
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function isEditingByAnotherOne()
    {
        // The user doesn't block himself
        if ($this->user->getId() === $this->currentUser->getId()) {
            return false;
        }

        // After 30s without activity, the lock lost its effect
        if ($this->startDate->add(new \DateInterval(self::EXPIRATION_DURATION)) < new \DateTime()) {
            return false;
        }

        return true;
    }
}
