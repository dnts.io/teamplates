<?php

namespace AppBundle\Services\Lock;

use AppBundle\Document\Filling;

class FilledDocument
{
    const EXPIRATION_DURATION = 'PT30M';


    public function __construct()
    {

    }

    /**
     * @param Filling $filledDocument
     *
     * @return bool
     */
    public function isLock(Filling $filledDocument)
    {
        $lastUpdate = clone $filledDocument->getUpdateDate();

        if ($filledDocument->getStatus() === Filling::STATUS_LOCK) {
            if ($lastUpdate && $lastUpdate->add(new \DateInterval(self::EXPIRATION_DURATION)) > new \DateTime()) {
                return true;
            }
        }

        return false;
    }
}
