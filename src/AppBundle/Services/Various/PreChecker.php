<?php

namespace AppBundle\Services\Various;

use AppBundle\Document\Company;
use AppBundle\Document\Field;
use AppBundle\Document\Field\GroupField;
use AppBundle\Document\Filling;
use AppBundle\Document\Node;
use AppBundle\Document\Template;
use AppBundle\Document\User;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class PreChecker
{
    /** @var User */
    private $user;

    /**
     * PreChecker constructor.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @param Company $company
     */
    public function checkCompany($company)
    {
        if (!$company) {
            throw new NotFoundHttpException('This company does not exist');
        }

        if (!$this->user->getCompany()) {
            throw new NotFoundHttpException('You are not member of a company');
        }

        if ($company->getId() !== $this->user->getCompany()->getId()) {
            throw new AccessDeniedException('You are not member of the company');
        }
    }

    /**
     * @param Filling $filling
     */
    public function checkFilling($filling)
    {
        if (!$filling) {
            throw new NotFoundHttpException('This filling does not exist');
        }

        if ($filling->getFiller()->getId() !== $this->user->getId()) {
            throw new AccessDeniedException('You are not owner of the filling');
        }
    }

    /**
     * @param Field $field
     */
    public function checkField($field)
    {
        if (!$field) {
            throw new NotFoundHttpException('This field does not exist');
        }
    }

    /**
     * @param GroupField $groupField
     */
    public function checkGroupField($groupField)
    {
        if (!$groupField) {
            throw new NotFoundHttpException('This group field does not exist');
        }
    }

    /**
     * @param Node $node
     */
    public function checkNode($node)
    {
        if (!$node) {
            throw new NotFoundHttpException('This node does not exist');
        }

        // @todo: Find the template to check the owner of the node
    }
    /**
     * @param Template $template
     */
    public function checkTemplate($template)
    {
        if (!$template) {
            throw new NotFoundHttpException('This template does not exist');
        }

        if ($template->getOwner()->getId() !== $this->user->getId()) {
            throw new AccessDeniedException('You are not owner of the template');
        }
    }
}
