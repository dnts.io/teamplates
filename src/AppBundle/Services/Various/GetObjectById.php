<?php

namespace AppBundle\Services\Various;

use AppBundle\Document\Company;
use AppBundle\Document\Field;
use AppBundle\Document\Field\GroupField;
use AppBundle\Document\Filling;
use AppBundle\Document\Node;
use AppBundle\Document\Template;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\DocumentRepository;

class GetObjectById
{
    /** @var ObjectManager */
    private $manager;

    /** @var PreChecker */
    private $preChecker;

    /**
     * GetObjectById constructor.
     *
     * @param ObjectManager $manager
     * @param PreChecker    $preChecker
     */
    public function __construct(ObjectManager $manager, PreChecker $preChecker)
    {
        $this->manager    = $manager;
        $this->preChecker = $preChecker;
    }

    /**
     * @param string $id
     * @param string $class
     * @param string $checkMethod
     *
     * @return mixed
     */
    private function get($id, $class, $checkMethod)
    {
        $object = $this->manager->getRepository($class)->find($id);
        call_user_func([$this->preChecker, $checkMethod], $object);

        return $object;
    }

    /**
     * @param string $id
     *
     * @return Company
     */
    public function getCompany($id)
    {
        return $this->get($id, Company::class, 'checkCompany');
    }

    /**
     * @param string $id
     *
     * @return Field
     */
    public function getField($id)
    {
        return $this->get($id, Field::class, 'checkField');
    }

    /**
     * @param string $id
     *
     * @return Filling
     */
    public function getFilling($id)
    {
        return $this->get($id, Filling::class, 'checkFilling');
    }

    /**
     * @param string $id
     *
     * @return GroupField
     */
    public function getGroupField($id)
    {
        return $this->get($id, GroupField::class, 'checkGroupField');
    }

    /**
     * @param string $id
     *
     * @return Node
     */
    public function getNode($id)
    {
        return $this->get($id, Node::class, 'checkNode');
    }

    /**
     * @param string $id
     *
     * @return Template
     */
    public function getTemplate($id)
    {
        return $this->get($id, Template::class, 'checkTemplate');
    }

}
