<?php

namespace AppBundle\Services\Field;

use Symfony\Component\HttpFoundation\Request;

class Form
{

    /**
     * @var array
     */
    private $arrayForm;

    /**
     * Form constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $form = [];
        parse_str($request->get('form'), $form);

        $this->arrayForm = $form;
    }

    /**
     * @param $key
     *
     * @return mixed|null
     */
    public function get($key)
    {
        if (!isset($this->arrayForm[$key])) {
            return null;
        }

        return $this->arrayForm[$key];
    }
}
