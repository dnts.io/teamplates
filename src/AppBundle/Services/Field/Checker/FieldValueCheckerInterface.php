<?php

namespace AppBundle\Services\Field\Checker;


interface FieldValueCheckerInterface
{
    /**
     * @return void
     */
    public function validate();

    /**
     * @return bool
     */
    public function isValid() : bool;

    /**
     * @return array
     */
    public function getErrors() : array;
}
