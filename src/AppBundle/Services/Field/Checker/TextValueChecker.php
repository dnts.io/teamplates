<?php

namespace AppBundle\Services\Field\Checker;

use AppBundle\Document\Field\Text;

class TextValueChecker extends BasicValueChecker
{
    /**
     * This is only for auto completion purpose
     * @var Text
     */
    protected $field;

    public function validate()
    {
        parent::validate();

        $valueLength = mb_strlen($this->value);

        if ($valueLength < $this->field->getMinLength()) {
            $this->errors[] = 'Min '.$this->field->getMinLength().' characters';
        }

        if ($valueLength > $this->field->getMaxLength()) {
            $this->errors[] = 'Max '.$this->field->getMaxLength().' characters';
        }

        if (!$this->field->getAllowNumeric() && preg_match('/[0-9]+/', $this->value)) {
            $this->errors[] = 'Numeric not allowed';
        }

        $specialCharsPattern = preg_quote('#$%^&*()+=-[]\';,./{}|\":<>?~', '#');
        if (!$this->field->getAllowSpecialChars() && preg_match("#[{$specialCharsPattern}]#", $this->value)) {
            $this->errors[] = 'Special characters not allowed';
        }

        if ($this->field->getCaseType() === 'upper' && preg_match('/[a-z]+/', $this->value)) {
            $this->errors[] = 'Only upper case characters';
        }

        if ($this->field->getCaseType() === 'lower' && preg_match('/[A-Z]+/', $this->value)) {
            $this->errors[] = 'Only lower case characters';
        }
    }
}
