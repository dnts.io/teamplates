<?php

namespace AppBundle\Services\Field\Checker;

use AppBundle\Document\Field\FBoolean;

class BooleanValueChecker extends BasicValueChecker
{
    /**
     * This is only for auto completion purpose
     * @var FBoolean
     */
    protected $field;

    public function validate()
    {
        parent::validate();
    }
}
