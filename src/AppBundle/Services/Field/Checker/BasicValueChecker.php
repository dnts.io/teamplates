<?php

namespace AppBundle\Services\Field\Checker;

use AppBundle\Document\Field;

class BasicValueChecker implements FieldValueCheckerInterface
{
    /**
     * @var string
     */
    protected $value;

    /**
     * @var array
     */
    protected $errors;

    /**
     * @var Field
     */
    protected $field;

    /**
     * BasicValueChecker constructor.
     *
     * @param Field  $field
     * @param string $value
     */
    public function __construct(Field $field, string $value)
    {
        $this->field  = $field;
        $this->value  = $value;
        $this->errors = [];
    }

    /**
     *
     */
    public function validate()
    {
        $valueLength = mb_strlen($this->value);

        if ($this->field->getIsRequired() && $valueLength === 0) {
            $this->errors[] = 'This field is required';
        }

        // TODO Plage de date pour la validité
    }

    /**
     * @return bool
     */
    public function isValid() : bool
    {
        $this->validate();

        return empty($this->errors);
    }

    /**
     * @return array
     */
    public function getErrors() : array
    {
        return $this->errors;
    }
}
