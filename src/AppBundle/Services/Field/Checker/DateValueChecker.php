<?php

namespace AppBundle\Services\Field\Checker;

use AppBundle\Document\Field\Date;

class DateValueChecker extends BasicValueChecker
{
    /**
     * This is only for auto completion purpose
     * @var Date
     */
    protected $field;

    public function validate()
    {
        parent::validate();

        if (!$this->value) {
            return;
        }

        $this->value = \DateTime::createFromFormat('d/m/Y', $this->value);

        if ($this->field->getMinDate() && $this->value < $this->field->getMinDate()) {
            $this->errors[] = 'Min date : '.$this->field->getMinDate()->format('d/m/Y');
        }

        if ($this->field->getMaxDate() && $this->value > $this->field->getMaxDate()) {
            $this->errors[] = 'Max date : '.$this->field->getMaxDate()->format('d/m/Y');
        }
    }
}
