<?php

namespace AppBundle\Services\Field\Checker;

use AppBundle\Document\Field\FNumeric;

class NumericValueChecker extends BasicValueChecker
{
    /**
     * This is only for auto completion purpose
     * @var FNumeric
     */
    protected $field;

    public function validate()
    {
        parent::validate();

        if (!is_numeric($this->value)) {
            $this->errors[] = 'Only numerics allowed';
        }

        if (is_numeric($this->field->getMin()) && $this->value < $this->field->getMin()) {
            $this->errors[] = 'Min : '.$this->field->getMin();
        }

        if (is_numeric($this->field->getMax()) && $this->value > $this->field->getMax()) {
            $this->errors[] = 'Max :'.$this->field->getMax();
        }

        if (!$this->field->getAllowDecimal() && preg_match('/^[0-9]+\.[0-9]+$/', $this->value)) {
            $this->errors[] = 'Decimal not allowed';
        }
    }
}
