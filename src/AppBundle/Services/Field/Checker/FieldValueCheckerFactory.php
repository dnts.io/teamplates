<?php

namespace AppBundle\Services\Field\Checker;

use AppBundle\Document\Field;

class FieldValueCheckerFactory
{
    /**
     * @var mixed|null|string
     */
    protected $value;

    /**
     * @var array
     */
    protected $typeClassCheckerMapping = [
        'text'    => TextValueChecker::class,
        'numeric' => NumericValueChecker::class,
        'age'     => AgeValueChecker::class,
        'date'    => DateValueChecker::class,
        'boolean' => BooleanValueChecker::class,
    ];


    /**
     * FieldValueCheckerFactory constructor.
     *
     * @param $value
     */
    public function __construct($value)
    {
        $this->value = $value;
    }


    /**
     * @param Field $field
     *
     * @return FieldValueCheckerInterface
     */
    public function create(Field $field): FieldValueCheckerInterface
    {
        $fieldType = $field->getPublicFieldType();

        return new $this->typeClassCheckerMapping[$fieldType]($field, $this->value);
    }
}
