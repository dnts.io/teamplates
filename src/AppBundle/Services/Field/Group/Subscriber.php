<?php

namespace AppBundle\Services\Field\Group;

use AppBundle\Document\Field\GroupField;
use AppBundle\Document\Field\Text;
use AppBundle\Services\Field\Form;

class Subscriber
{
    /**
     * @var array
     */
    protected $textFields;

    /**
     * @var GroupField
     */
    protected $groupField;

    /**
     * @var Form
     */
    private $form;

    public function __construct(Form $form)
    {
        $this->form = $form;
    }

    public function createFields()
    {
        $fieldNames           = [
            'name',
            'firstname',
            'maiden-name',
            'birthdate',
            'family-situation',
            'client-number',
            'private-phone',
            'professional-phone',
            'address',
            'fiscal-address',
            'email',
        ];
        $slufigyfiedGroupName = $this->slugifyGroupName();

        foreach ($fieldNames as $fieldName) {

            $textField = new Text();
            $textField->setName($slufigyfiedGroupName.'-'.$fieldName);
            $textField->setMinLength(2);
            $textField->setMaxLength(30);
            $textField->setCaseType('both');
            $textField->setIsRequired(true);
            $textField->setAllowNumeric(true);
            $textField->setAllowSpecialChars(true);

            $this->textFields[] = $textField;
            $this->groupField->addField($textField);
        }

        return $this->textFields;
    }


    public function createGroup()
    {
        $this->groupField = new GroupField();
        $this->groupField->setName($this->form->get('field-name'));

        return $this->groupField;

    }

    protected function slugifyGroupName()
    {

        $rule           = 'NFD; [:Nonspacing Mark:] Remove; NFC';
        $transliterator = \Transliterator::create($rule);
        $string         = $transliterator->transliterate($this->form->get('field-name'));

        return preg_replace(
            '/[^a-z0-9]/',
            '-',
            strtolower(trim(strip_tags($string)))
        );
    }
}
