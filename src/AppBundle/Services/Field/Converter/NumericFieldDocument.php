<?php

namespace AppBundle\Services\Field\Converter;

use AppBundle\Document\Field\FNumeric;

class NumericFieldDocument extends BasicFieldDocument
{
    /**
     * @var FNumeric
     */
    protected $document;

    public function convert()
    {
        parent::convert();

        $this->document->setAllowDecimal($this->form->get('field-numeric-is-decimal-accepted') === 'on');

        $min = $this->form->get('field-numeric-min');
        if(is_numeric($min)) {
            $this->document->setMin($min);
        }

        $max = $this->form->get('field-numeric-max');
        if(is_numeric($max)) {
            $this->document->setMax($max);
        }
    }
}
