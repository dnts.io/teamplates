<?php

namespace AppBundle\Services\Field\Converter;

use AppBundle\Document\Field\Date;

class DateFieldDocument extends BasicFieldDocument
{
    const DATE_FORMAT = 'd/m/Y';

    /** @var Date */
    protected $document;

    public function convert()
    {
        parent::convert();

        $this->document->setMinDate($this->convertDate($this->form->get('field-date-min')));
        $this->document->setMaxDate($this->convertDate($this->form->get('field-date-max')));
    }

    /**
     * @param $dateString
     *
     * @return bool|\DateTime
     */
    private function convertDate($dateString)
    {
        $dateTime = \DateTime::createFromFormat(self::DATE_FORMAT, $dateString);
        if (!$dateTime) {
            return null;
        }

        return $dateTime;
    }
}
