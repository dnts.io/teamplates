<?php

namespace AppBundle\Services\Field\Converter;

use AppBundle\Document\Field;
use AppBundle\Services\Field\Form;

class BasicFieldDocument
{
    /**
     * @var Form
     */
    protected $form;

    /**
     * @var Field
     */
    protected $document;

    public function __construct(Form $form, Field $document)
    {
        $this->form     = $form;
        $this->document = $document;
    }


    public function convert()
    {
        $this->document->setName($this->form->get('field-name'));
        $isRequired = $this->form->get('is-required') === 'on';
        $this->document->setIsRequired($isRequired);
        $isLimitedOnTime = $this->form->get('is-limited-on-time') === 'on';
        $this->document->setIsLimitedInTime($isLimitedOnTime);
        if ($isLimitedOnTime) {
            $this->document->setLimitedDateFrom($this->form->get('time-range-from'));
            $this->document->setLimitedDateTo($this->form->get('time-range-to'));
        }
        $this->document->setDefaultValue($this->form->get('field-default-value'));
    }

    /**
     * @return Field
     */
    public function getDocument()
    {
        return $this->document;
    }
}
