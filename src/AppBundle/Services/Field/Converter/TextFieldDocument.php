<?php

namespace AppBundle\Services\Field\Converter;

use AppBundle\Document\Field\Text;

class TextFieldDocument extends BasicFieldDocument
{
    /**
     * @var Text
     */
    protected $document;

    public function convert()
    {
        parent::convert();

        $this->document->setAllowSpecialChars($this->form->get('field-text-is-special-chars-accepted') === 'on');
        $this->document->setAllowNumeric($this->form->get('field-text-is-numeric-accepted') === 'on');
        $this->document->setCaseType($this->form->get('field-text-case'));
        $this->document->setMaxLength($this->form->get('field-text-max'));
        $this->document->setMinLength($this->form->get('field-text-min'));
    }
}
