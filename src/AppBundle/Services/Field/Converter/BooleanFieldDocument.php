<?php

namespace AppBundle\Services\Field\Converter;

use AppBundle\Document\Field\FBoolean;

class BooleanFieldDocument extends BasicFieldDocument
{
    /**
     * @var FBoolean
     */
    protected $document;

    public function convert()
    {
        parent::convert();
    }
}
