<?php

namespace AppBundle\Services\Field\Converter;

use AppBundle\Document\Field\Age;

class AgeFieldDocument extends NumericFieldDocument
{
    /**
     * @var Age
     */
    protected $document;

    public function convert()
    {
        parent::convert();

        $this->document->setAllowDecimal(false);
    }
}
