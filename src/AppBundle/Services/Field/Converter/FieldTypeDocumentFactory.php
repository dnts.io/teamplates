<?php

namespace AppBundle\Services\Field\Converter;

use AppBundle\Document\Field\Age;
use AppBundle\Document\Field\Date;
use AppBundle\Document\Field\FBoolean;
use AppBundle\Document\Field\FNumeric;
use AppBundle\Document\Field\Text;
use AppBundle\Services\Field\Form;

class FieldTypeDocumentFactory
{
    /**
     * @var Form
     */
    protected $form;

    /**
     * @var mixed|null|string
     */
    protected $fieldType;

    /**
     * @var array
     */
    protected $typeClassMapping = [
        'text'    => [
            'fieldDocument' => TextFieldDocument::class,
            'document'      => Text::class,
        ],
        'numeric' => [
            'fieldDocument' => NumericFieldDocument::class,
            'document'      => FNumeric::class,
        ],
        'age'     => [
            'fieldDocument' => AgeFieldDocument::class,
            'document'      => Age::class,
        ],
        'date'    => [
            'fieldDocument' => DateFieldDocument::class,
            'document'      => Date::class,
        ],
        'boolean' => [
            'fieldDocument' => BooleanFieldDocument::class,
            'document'      => FBoolean::class,
        ],
    ];


    public function __construct(Form $form)
    {
        $this->form      = $form;
        $this->fieldType = $form->get('field-type');
    }

    /**
     * @return BasicFieldDocument
     */
    public function create()
    {
        $typeMapping   = $this->typeClassMapping[$this->fieldType];
        $fieldDocument = $typeMapping['fieldDocument'];
        $document      = $typeMapping['document'];

        return new $fieldDocument($this->form, new $document());
    }
}
