<?php

namespace AppBundle\Services\Field\Validator;

class TextValidator extends Basic
{

    public function validate()
    {
        parent::validate();

        if (!$this->isMinAndMaxLengthLogic()) {
            $this->errors[] = $this->translator->trans('field.error.text.length');
        }

        if (!$this->isLengthPositive()) {
            $this->errors[] = $this->translator->trans('field.error.text.lengthnotpositive');
        }

        if (!$this->isOnlyNumericOnLength()) {
            $this->errors[] = $this->translator->trans('field.error.text.onlynumeric');
        }

        if (!$this->isCaseSelected()) {
            $this->errors[] = $this->translator->trans('field.error.empty.case');
        } else {
            if (!$this->isCaseValueAllowed()) {
                $this->errors[] = $this->translator->trans('field.error.case.invalid');
            }
        }
    }

    /**
     * @return bool
     */
    protected function isMinAndMaxLengthLogic() : bool
    {
        return $this->form->get('field-text-min') <= $this->form->get('field-text-max');
    }

    /**
     * @return bool
     */
    protected function isCaseSelected() : bool
    {
        return !empty($this->form->get('field-text-case'));
    }

    /**
     * @return bool
     */
    protected function isCaseValueAllowed() : bool
    {
        $allowedValue = ['both', 'upper', 'lower']; // @TODO move this from here maybe ?
        return in_array($this->form->get('field-text-case'), $allowedValue);
    }

    protected function isLengthPositive()
    {
        return $this->form->get('field-text-min') >= 0 && $this->form->get('field-text-max') >= 0;
    }

    protected function isOnlyNumericOnLength()
    {
        return ctype_digit($this->form->get('field-text-min')) && ctype_digit($this->form->get('field-text-max'));
    }

    // TODO pas de caractère autre qu'un chiffre dans le input
}
