<?php

namespace AppBundle\Services\Field\Validator;

use AppBundle\Services\Field\Form;
use Symfony\Component\Translation\TranslatorInterface;

class FieldTypeValidatorFactory
{
    /**
     * @var Form
     */
    protected $form;

    /**
     * @var mixed|null|string
     */
    protected $fieldType;

    /**
     * @var array
     */
    protected $typeClassValidatorMapping = [
        'basic'   => Basic::class,
        'text'    => TextValidator::class,
        'numeric' => NumericValidator::class,
        'age'     => AgeValidator::class,
        'date'    => DateValidator::class,
        'boolean' => BooleanValidator::class,
    ];

    /**
     * FieldTypeValidatorFactory constructor.
     *
     * @param Form                $form
     * @param TranslatorInterface $translator
     */
    public function __construct(Form $form, TranslatorInterface $translator)
    {
        $this->translator = $translator;
        $this->form       = $form;
        $this->fieldType  = $form->get('field-type') ? $form->get('field-type') : 'basic';
    }

    /**
     * @return FieldTypeValidatorInterface
     */
    public function create(): FieldTypeValidatorInterface
    {
        return new $this->typeClassValidatorMapping[$this->fieldType]($this->form, $this->translator);
    }
}
