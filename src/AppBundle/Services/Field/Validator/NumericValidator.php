<?php

namespace AppBundle\Services\Field\Validator;

class NumericValidator extends Basic
{

    public function validate()
    {
        parent::validate();

        if($this->isMinAndMaxDefined()) {

            if (!$this->isMinAndMaxLogic()) {
                $this->errors[] = $this->translator->trans('field.error.text.length');
            }

            if (!$this->isMinAndMaxNumerics()) {
                $this->errors[] = $this->translator->trans('field.error.text.onlynumeric');
            }
        }
    }

    /**
     * @return bool
     */
    protected function isMinAndMaxDefined() : bool
    {
        return $this->form->get('field-numeric-min') && $this->form->get('field-numeric-max');
    }

    /**
     * @return bool
     */
    protected function isMinAndMaxLogic() : bool
    {
        return $this->form->get('field-numeric-min') <= $this->form->get('field-numeric-max');
    }

    /**
     * @return bool
     */
    protected function isMinAndMaxNumerics() : bool
    {
        return is_numeric($this->form->get('field-numeric-min')) && is_numeric($this->form->get('field-numeric-max'));
    }
}
