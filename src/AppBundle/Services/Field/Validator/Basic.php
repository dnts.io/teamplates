<?php

namespace AppBundle\Services\Field\Validator;

use AppBundle\Services\Field\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Translation\TranslatorInterface;

class Basic implements FieldTypeValidatorInterface
{
    /**
     * @var array
     */
    protected $errors;

    /**
     * @var Request
     */
    protected $form;

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * BasicValidator constructor.
     *
     * @param Form                $form
     * @param TranslatorInterface $translator
     */
    public function __construct(Form $form, TranslatorInterface $translator)
    {
        $this->form       = $form;
        $this->translator = $translator;
    }

    /**
     *
     */
    public function validate()
    {
        if ($this->isFieldNameEmpty()) {
            $this->errors[] = $this->translator->trans('field.error.empty.fieldname');
        }

        if ($this->isFieldTypeNotSelected()) {
            $this->errors[] = $this->translator->trans('field.error.empty.fieldtype');
        }
    }

    /**
     * @return bool
     */
    public function isValid() : bool
    {
        $this->validate();

        return empty($this->errors);
    }

    /**
     * @return bool
     */
    protected function isFieldNameEmpty()
    {
        $fieldName = trim($this->form->get('field-name'));

        return empty($fieldName);
    }

    /**
     * @return bool
     */
    protected function isFieldTypeNotSelected()
    {
        $fieldType = trim($this->form->get('field-type'));

        return empty($fieldType);
    }

    /**
     * @return array
     */
    public function getErrors() : array
    {
        return $this->errors;
    }
}
