<?php

namespace AppBundle\Services\Field\Validator;


interface FieldTypeValidatorInterface
{
    /**
     * @return void
     */
    public function validate();

    /**
     * @return bool
     */
    public function isValid() : bool;

    /**
     * @return array
     */
    public function getErrors() : array;
}
