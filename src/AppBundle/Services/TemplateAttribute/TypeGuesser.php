<?php

namespace AppBundle\Services\TemplateAttribute;


class TypeGuesser
{
    private $attribute;

    private $availableAttributes = ['domain', 'type', 'keyword', 'category'];

    public function setType($attribute)
    {
        $this->attribute = $attribute;
    }

    /**
     * Will determine the Document class name to load based on the attribute type (default to Type)
     *
     * @return string
     */
    public function determineDocumentClass()
    {
        return in_array($this->attribute, $this->availableAttributes) ? ucfirst($this->attribute) : 'Domain';
    }

    /**
     * Will determine the Form Type class name to load based on the attribute type (default to TypeType)
     *
     * @return string
     */
    public function determineFormClass()
    {
        return in_array($this->attribute, $this->availableAttributes) ? ucfirst($this->attribute).'Type' : 'DomainType';
    }
}
