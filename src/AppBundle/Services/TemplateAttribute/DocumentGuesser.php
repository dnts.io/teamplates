<?php

namespace AppBundle\Services\TemplateAttribute;


class DocumentGuesser
{
    /**
     * @var TypeGuesser
     */
    private $typeGuesser;

    /**
     * @param TypeGuesser $typeGuesser
     *
     */
    public function setTypeGuesser(TypeGuesser $typeGuesser)
    {
        $this->typeGuesser = $typeGuesser;
    }

    /**
     * @return mixed
     */
    public function getDocument()
    {
        $documentClassName = 'AppBundle\Document\\'.$this->typeGuesser->determineDocumentClass();

        return new $documentClassName();
    }

    /**
     * @return string
     */
    public function getDocumentName()
    {
        return $this->typeGuesser->determineDocumentClass();
    }
}
