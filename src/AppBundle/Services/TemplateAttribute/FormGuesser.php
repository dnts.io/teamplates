<?php

namespace AppBundle\Services\TemplateAttribute;


class FormGuesser
{
    /**
     * @var TypeGuesser
     */
    private $typeGuesser;

    /**
     * @param TypeGuesser $typeGuesser
     */
    public function setTypeGuesser(TypeGuesser $typeGuesser)
    {
        $this->typeGuesser = $typeGuesser;
    }

    /**
     * @return string
     */
    public function getFormClass()
    {
        return 'AppBundle\Form\\'.$this->typeGuesser->determineFormClass();
    }
}
