<?php

namespace AppBundle\EventListener;

use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class RegistrationListener implements EventSubscriberInterface
{

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            FOSUserEvents::REGISTRATION_INITIALIZE => 'onRegistrationInitializing',
        ];
    }

    /**
     * Disable the created user
     *
     * @param GetResponseUserEvent $event
     */
    public function onRegistrationInitializing(GetResponseUserEvent $event)
    {
        $event->getUser()->setEnabled(false);
    }
}
