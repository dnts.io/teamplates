<?php

namespace AppBundle\Document\Repository;

use Doctrine\ODM\MongoDB\DocumentRepository;

class SearchRepository extends DocumentRepository
{
    public function searchByName($name)
    {
        return $this->createQueryBuilder()
                    ->field('name')->equals(new \MongoRegex('/^'.$name.'/i'))
                    ->limit(10)
                    ->getQuery();
    }
}
