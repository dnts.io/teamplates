<?php

namespace AppBundle\Document\Field;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document
 */
class Age extends FNumeric
{
    protected $publicFieldType = 'age'; // we have to put it in subclass

    public function __construct()
    {
        parent::__construct();

        $this->publicFieldType = 'age';
        $this->allowDecimal    = false;
    }
}
