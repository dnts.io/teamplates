<?php

namespace AppBundle\Document\Field;

use AppBundle\Document\Field;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document
 */
class GroupField extends Field
{

    /**
     * @MongoDB\ReferenceMany(targetDocument="AppBundle\Document\Field")
     */
    protected $fields;

    protected $publicFieldType = 'group'; // we have to put it in subclass


    public function __construct()
    {
        $this->fields          = new ArrayCollection();
        $this->publicFieldType = 'group';
    }

    /**
     * Add field
     *
     * @param Field $field
     */
    public function addField(Field $field)
    {
        $this->fields[] = $field;
    }

    /**
     * Remove field
     *
     * @param Field $field
     */
    public function removeField(Field $field)
    {
        $this->fields->removeElement($field);
    }

    /**
     * Get fields
     *
     * @return \Doctrine\Common\Collections\Collection $fields
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * Set publicFieldType
     *
     * @param string $publicFieldType
     * @return $this
     */
    public function setPublicFieldType($publicFieldType)
    {
        $this->publicFieldType = $publicFieldType;
        return $this;
    }

    /**
     * Get publicFieldType
     *
     * @return string $publicFieldType
     */
    public function getPublicFieldType()
    {
        return $this->publicFieldType;
    }
}
