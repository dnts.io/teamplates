<?php

namespace AppBundle\Document\Field;

use AppBundle\Document\Field;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document
 */
class Date extends Field
{
    /**
     * @MongoDB\Field(type="date")
     */
    protected $minDate;

    /**
     * @MongoDB\Field(type="date")
     */
    protected $maxDate;

    protected $publicFieldType = 'date'; // we have to put it in subclass

    public function __construct()
    {
        $this->publicFieldType = 'date';
    }

    /**
     * Set minDate
     *
     * @param \DateTime $minDate
     * @return $this
     */
    public function setMinDate($minDate)
    {
        $this->minDate = $minDate;
        return $this;
    }

    /**
     * Get minDate
     *
     * @return \DateTime $minDate
     */
    public function getMinDate()
    {
        return $this->minDate;
    }

    /**
     * Set maxDate
     *
     * @param \DateTime $maxDate
     * @return $this
     */
    public function setMaxDate($maxDate)
    {
        $this->maxDate = $maxDate;
        return $this;
    }

    /**
     * Get maxDate
     *
     * @return \DateTime $maxDate
     */
    public function getMaxDate()
    {
        return $this->maxDate;
    }
}
