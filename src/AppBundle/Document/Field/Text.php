<?php

namespace AppBundle\Document\Field;

use AppBundle\Document\Field;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document
 */
class Text extends Field
{
    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $allowSpecialChars;

    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $allowNumeric;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $caseType;

    /**
     * @MongoDB\Field(type="integer")
     */
    protected $maxLength;

    /**
     * @MongoDB\Field(type="integer")
     */
    protected $MinLength;

    protected $publicFieldType = 'text'; // we have to put it in subclass

    public function __construct()
    {
        $this->publicFieldType = 'text';
    }

    /**
     * Set allowSpecialChars
     *
     * @param boolean $allowSpecialChars
     *
     * @return $this
     */
    public function setAllowSpecialChars($allowSpecialChars)
    {
        $this->allowSpecialChars = $allowSpecialChars;

        return $this;
    }

    /**
     * Get allowSpecialChars
     *
     * @return boolean $allowSpecialChars
     */
    public function getAllowSpecialChars()
    {
        return $this->allowSpecialChars;
    }

    /**
     * Set allowNumeric
     *
     * @param boolean $allowNumeric
     *
     * @return $this
     */
    public function setAllowNumeric($allowNumeric)
    {
        $this->allowNumeric = $allowNumeric;

        return $this;
    }

    /**
     * Get allowNumeric
     *
     * @return boolean $allowNumeric
     */
    public function getAllowNumeric()
    {
        return $this->allowNumeric;
    }

    /**
     * Set caseType
     *
     * @param string $caseType
     *
     * @return $this
     */
    public function setCaseType($caseType)
    {
        $this->caseType = $caseType;

        return $this;
    }

    /**
     * Get caseType
     *
     * @return string $caseType
     */
    public function getCaseType()
    {
        return $this->caseType;
    }

    /**
     * Set maxLength
     *
     * @param integer $maxLength
     *
     * @return $this
     */
    public function setMaxLength($maxLength)
    {
        $this->maxLength = $maxLength;

        return $this;
    }

    /**
     * Get maxLength
     *
     * @return integer $maxLength
     */
    public function getMaxLength()
    {
        return $this->maxLength;
    }

    /**
     * Set minLength
     *
     * @param integer $minLength
     *
     * @return $this
     */
    public function setMinLength($minLength)
    {
        $this->MinLength = $minLength;

        return $this;
    }

    /**
     * Get minLength
     *
     * @return integer $minLength
     */
    public function getMinLength()
    {
        return $this->MinLength;
    }

    /**
     * Set publicFieldType
     *
     * @param string $publicFieldType
     * @return $this
     */
    public function setPublicFieldType($publicFieldType)
    {
        $this->publicFieldType = $publicFieldType;
        return $this;
    }

    /**
     * Get publicFieldType
     *
     * @return string $publicFieldType
     */
    public function getPublicFieldType()
    {
        return $this->publicFieldType;
    }
}
