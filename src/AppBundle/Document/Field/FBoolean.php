<?php

namespace AppBundle\Document\Field;

use AppBundle\Document\Field;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * FBoolean ("Boolean" canno't be used as class name) because 'Field Boolean'
 * @MongoDB\Document
 */
class FBoolean extends Field
{
    protected $publicFieldType = 'boolean'; // we have to put it in subclass

    public function __construct()
    {
        $this->publicFieldType = 'boolean';
    }
}
