<?php

namespace AppBundle\Document\Field;

use AppBundle\Document\Field;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * FNumeric ("Numeric" canno't be used as clas name) because 'Field Numeric'
 * @MongoDB\Document
 */
class FNumeric extends Field
{
    /**
     * @MongoDB\Field(type="integer")
     */
    protected $max;

    /**
     * @MongoDB\Field(type="integer")
     */
    protected $min;

    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $allowDecimal;

    protected $publicFieldType = 'numeric'; // we have to put it in subclass

    public function __construct()
    {
        $this->publicFieldType = 'numeric';
    }

    /**
     * Set max
     *
     * @param integer $max
     * @return $this
     */
    public function setMax($max)
    {
        $this->max = $max;
        return $this;
    }

    /**
     * Get max
     *
     * @return integer $max
     */
    public function getMax()
    {
        return $this->max;
    }

    /**
     * Set min
     *
     * @param integer $min
     * @return $this
     */
    public function setMin($min)
    {
        $this->min = $min;
        return $this;
    }

    /**
     * Get min
     *
     * @return integer $min
     */
    public function getMin()
    {
        return $this->min;
    }

    /**
     * Set allowDecimal
     *
     * @param boolean $allowDecimal
     * @return $this
     */
    public function setAllowDecimal($allowDecimal)
    {
        $this->allowDecimal = $allowDecimal;
        return $this;
    }

    /**
     * Get allowDecimal
     *
     * @return boolean $allowDecimal
     */
    public function getAllowDecimal()
    {
        return $this->allowDecimal;
    }
}
