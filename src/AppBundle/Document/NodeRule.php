<?php

namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document
 */
class NodeRule
{
    /**
     * @MongoDB\Id
     */
    protected $id;

    /**
     * @MongoDB\ReferenceOne(targetDocument="Field")
     */
    protected $field;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $rule;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $value;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $logic;


    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set field
     *
     * @param Field $field
     * @return $this
     */
    public function setField(Field $field)
    {
        $this->field = $field;
        return $this;
    }

    /**
     * Get field
     *
     * @return Field $field
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * Set rule
     *
     * @param string $rule
     * @return $this
     */
    public function setRule($rule)
    {
        $this->rule = $rule;
        return $this;
    }

    /**
     * Get rule
     *
     * @return string $rule
     */
    public function getRule()
    {
        return $this->rule;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return $this
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * Get value
     *
     * @return string $value
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set logic
     *
     * @param string $logic
     * @return $this
     */
    public function setLogic($logic)
    {
        $this->logic = $logic;
        return $this;
    }

    /**
     * Get logic
     *
     * @return string $logic
     */
    public function getLogic()
    {
        return $this->logic;
    }
}
