<?php

namespace AppBundle\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @MongoDB\Document(repositoryClass="Gedmo\Tree\Document\MongoDB\Repository\MaterializedPathRepository")
 * @Gedmo\Tree(type="materializedPath", activateLocking=true)
 */
class Node
{
    const ACTION_POPUP = 'popup';
    const ACTION_DISPLAY = 'display';
    const ACTION_EMAIL = 'email';

    /**
     * @MongoDB\Id
     * @Gedmo\TreePathSource
     */
    protected $id;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $title;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $content;

    /**
     * @MongoDB\Field(type="string")
     * @Gedmo\TreePath(separator=".", appendId=false)
     */
    protected $path;

    /**
     * @Gedmo\TreeParent
     * @MongoDB\ReferenceOne(targetDocument="Node")
     */
    protected $parent;

    /**
     * @Gedmo\TreeLevel
     * @MongoDB\Field(type="int", nullable=true)
     */
    protected $level;

    /**
     * @Gedmo\TreeLockTime
     * @MongoDB\Field(type="date")
     */
    protected $lockTime;

    /**
     * @MongoDB\Field(type="hash")
     */
    protected $lock;

    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $validated;

    /**
     * @MongoDB\ReferenceMany(targetDocument="NodeRule")
     */
    protected $rules;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $ruleAction;

    /**
     * @MongoDB\Field(type="date")
     */
    protected $ruleValidityFrom;

    /**
     * @MongoDB\Field(type="date")
     */
    protected $ruleValidityTo;


    /**
     * @MongoDB\Field(type="integer")
     */
    protected $order;


    public function getId()
    {
        return $this->id;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setParent(Node $parent = null)
    {
        $this->parent = $parent;
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function getLevel()
    {
        return $this->level;
    }

    public function getPath()
    {
        return $this->path;
    }

    public function getLockTime()
    {
        return $this->lockTime;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return $this
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string $content
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return $this
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Set level
     *
     * @param int $level
     *
     * @return $this
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Set lockTime
     *
     * @param \DateTime $lockTime
     *
     * @return $this
     */
    public function setLockTime($lockTime)
    {
        $this->lockTime = $lockTime;

        return $this;
    }

    /**
     * Set lock
     *
     * @param array $lock
     *
     * @return $this
     */
    public function setLock($lock)
    {
        $this->lock = $lock;

        return $this;
    }

    /**
     * Get lock
     *
     * @return array $lock
     */
    public function getLock()
    {
        return $this->lock;
    }

    /**
     * Set validated
     *
     * @param bool $validated
     *
     * @return $this
     */
    public function setValidated($validated)
    {
        $this->validated = (bool) $validated;

        return $this;
    }

    /**
     * Get validated
     *
     * @return bool $validated
     */
    public function getValidated()
    {
        return $this->validated;
    }

    public function __construct()
    {
        $this->rules = new ArrayCollection();
    }

    /**
     * Add rule
     *
     * @param NodeRule $rule
     */
    public function addRule(NodeRule $rule)
    {
        $this->rules[] = $rule;
    }

    /**
     * Remove rule
     *
     * @param NodeRule $rule
     */
    public function removeRule(NodeRule $rule)
    {
        $this->rules->removeElement($rule);
    }

    /**
     * Get rules
     *
     * @return \Doctrine\Common\Collections\Collection $rules
     */
    public function getRules()
    {
        return $this->rules;
    }

    /**
     * Set order
     *
     * @param integer $order
     *
     * @return $this
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return integer $order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set ruleAction
     *
     * @param string $ruleAction
     * @return $this
     */
    public function setRuleAction($ruleAction)
    {
        $this->ruleAction = $ruleAction;
        return $this;
    }

    /**
     * Get ruleAction
     *
     * @return string $ruleAction
     */
    public function getRuleAction()
    {
        return $this->ruleAction;
    }

    /**
     * Set ruleValidityFrom
     *
     * @param \DateTime $ruleValidityFrom
     * @return $this
     */
    public function setRuleValidityFrom($ruleValidityFrom)
    {
        $this->ruleValidityFrom = $ruleValidityFrom;
        return $this;
    }

    /**
     * Get ruleValidityFrom
     *
     * @return \DateTime $ruleValidityFrom
     */
    public function getRuleValidityFrom()
    {
        return $this->ruleValidityFrom;
    }

    /**
     * Set ruleValidityTo
     *
     * @param date $ruleValidityTo
     * @return $this
     */
    public function setRuleValidityTo($ruleValidityTo)
    {
        $this->ruleValidityTo = $ruleValidityTo;
        return $this;
    }

    /**
     * Get ruleValidityTo
     *
     * @return date $ruleValidityTo
     */
    public function getRuleValidityTo()
    {
        return $this->ruleValidityTo;
    }
}
