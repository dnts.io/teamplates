<?php

namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document
 */
class Filling
{
    const STATUS_LOCK = 'locked';
    const STATUS_UNLOCK = 'unlocked';

    /**
     * @MongoDB\Id
     */
    protected $id;

    /**
     * @MongoDB\ReferenceOne(targetDocument="User")
     */
    protected $filler;

    /**
     * @MongoDB\Field(type="date")
     */
    protected $creationDate;

    /**
     * @MongoDB\Field(type="date")
     */
    protected $updateDate;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $internalRef;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $status;

    /**
     * @MongoDB\ReferenceOne(targetDocument="Template")
     */
    protected $template;

    /**
     * @MongoDB\Field(type="hash")
     */
    protected $values;

    /**
     * @MongoDB\Field(type="hash")
     */
    protected $groupValue;


    /**
     * Get id
     *
     * @return string $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set filler
     *
     * @param User $filler
     *
     * @return $this
     */
    public function setFiller(User $filler)
    {
        $this->filler = $filler;

        return $this;
    }

    /**
     * Get filler
     *
     * @return User $filler
     */
    public function getFiller()
    {
        return $this->filler;
    }

    /**
     * Set template
     *
     * @param Template $template
     *
     * @return $this
     */
    public function setTemplate(Template $template)
    {
        $this->template = $template;

        return $this;
    }

    /**
     * Get template
     *
     * @return Template $template
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * Set values
     *
     * @param array $values
     *
     * @return $this
     */
    public function setValues(array $values)
    {
        $this->values = $values;

        return $this;
    }

    /**
     * Get values
     *
     * @return array $values
     */
    public function getValues()
    {
        return $this->values;
    }

    /**
     * Set groupValue
     *
     * @param array $groupValue
     *
     * @return $this
     */
    public function setGroupValue(array $groupValue)
    {
        $this->groupValue = $groupValue;

        return $this;
    }

    /**
     * Get groupValue
     *
     * @return array $groupValue
     */
    public function getGroupValue()
    {
        return $this->groupValue;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     *
     * @return $this
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime $creationDate
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set updateDate
     *
     * @param \DateTime $updateDate
     *
     * @return $this
     */
    public function setUpdateDate($updateDate)
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    /**
     * Get updateDate
     *
     * @return \DateTime $updateDate
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string $status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set internalRef
     *
     * @param string $internalRef
     *
     * @return $this
     */
    public function setInternalRef($internalRef)
    {
        $this->internalRef = $internalRef;

        return $this;
    }

    /**
     * Get internalRef
     *
     * @return string $internalRef
     */
    public function getInternalRef()
    {
        return $this->internalRef;
    }
}
