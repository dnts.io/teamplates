<?php

namespace AppBundle\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document
 */
class Template
{
    const STATUS_EDITABLE = 'editable';
    const STATUS_LOCK = 'locked';
    const STATUS_VALIDATED = 'validated';
    /**
     * @MongoDB\Id
     */
    protected $id;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $name;

    /**
     * @MongoDB\ReferenceOne(targetDocument="Company")
     */
    protected $company;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $internalReference;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $externalReference;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $language;

    /**
     * @MongoDB\ReferenceOne(targetDocument="Type")
     */
    protected $type;

    /**
     * @MongoDB\ReferenceOne(targetDocument="Category")
     */
    protected $category;

    /**
     * @MongoDB\ReferenceOne(targetDocument="Domain")
     */
    protected $domain;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $shortDescription;

    /**
     * @MongoDB\ReferenceMany(targetDocument="Keyword")
     */
    protected $keywords;

    /**
     * @MongoDB\Field(type="string", nullable=true)
     */
    protected $comment;

    /**
     * @MongoDB\ReferenceOne(targetDocument="User")
     */
    protected $owner;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $version;

    /**
     * @MongoDB\Field(type="date")
     */
    protected $creationDate;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $status;

    /**
     * @MongoDB\ReferenceOne(targetDocument="Node")
     */
    protected $alphaNode;

    /**
     * @MongoDB\ReferenceMany(targetDocument="Field")
     */
    protected $fields;

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set owner
     *
     * @param User $owner
     *
     * @return $this
     */
    public function setOwner(User $owner)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return User $owner
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set version
     *
     * @param string $version
     *
     * @return $this
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * Get version
     *
     * @return string $version
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Set description
     *
     * @param string $shortDescription
     *
     * @return $this
     */
    public function setShortDescription($shortDescription)
    {
        $this->shortDescription = $shortDescription;

        return $this;
    }

    /**
     * Get description
     *
     * @return string $description
     */
    public function getShortDescription()
    {
        return $this->shortDescription;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     *
     * @return $this
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime $creationDate
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string $status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set node
     *
     * @param Node $alphaNode
     *
     * @return $this
     */
    public function setAlphaNode(Node $alphaNode)
    {
        $this->alphaNode = $alphaNode;

        return $this;
    }

    /**
     * Get node
     *
     * @return Node $node
     */
    public function getAlphaNode()
    {
        return $this->alphaNode;
    }

    public function __construct()
    {
        $this->keywords = new ArrayCollection();
        $this->fields   = new ArrayCollection();
    }

    /**
     * Set internalReference
     *
     * @param string $internalReference
     *
     * @return $this
     */
    public function setInternalReference($internalReference)
    {
        $this->internalReference = $internalReference;

        return $this;
    }

    /**
     * Get internalReference
     *
     * @return string $internalReference
     */
    public function getInternalReference()
    {
        return $this->internalReference;
    }

    /**
     * Set externalReference
     *
     * @param string $externalReference
     *
     * @return $this
     */
    public function setExternalReference($externalReference)
    {
        $this->externalReference = $externalReference;

        return $this;
    }

    /**
     * Get externalReference
     *
     * @return string $externalReference
     */
    public function getExternalReference()
    {
        return $this->externalReference;
    }

    /**
     * Set language
     *
     * @param string $language
     *
     * @return $this
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return string $language
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set type
     *
     * @param Type $type
     *
     * @return $this
     */
    public function setType(Type $type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return Type $type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set category
     *
     * @param Category $category
     *
     * @return $this
     */
    public function setCategory(Category $category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return Category $category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set domain
     *
     * @param Domain $domain
     *
     * @return $this
     */
    public function setDomain(Domain $domain)
    {
        $this->domain = $domain;

        return $this;
    }

    /**
     * Get domain
     *
     * @return Domain $domain
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Add keyword
     *
     * @param Keyword $keyword
     */
    public function addKeyword(Keyword $keyword)
    {
        $this->keywords[] = $keyword;
    }

    /**
     * Remove keyword
     *
     * @param Keyword $keyword
     */
    public function removeKeyword(Keyword $keyword)
    {
        $this->keywords->removeElement($keyword);
    }

    /**
     * Get keywords
     *
     * @return \Doctrine\Common\Collections\Collection $keywords
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return $this
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string $comment
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Add field
     *
     * @param Field $field
     */
    public function addField(Field $field)
    {
        $this->fields[] = $field;
    }

    /**
     * Remove field
     *
     * @param Field $field
     */
    public function removeField(Field $field)
    {
        $this->fields->removeElement($field);
    }

    /**
     * Get fields
     *
     * @return \Doctrine\Common\Collections\Collection $fields
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * Set company
     *
     * @param AppBundle\Document\Company $company
     * @return $this
     */
    public function setCompany(\AppBundle\Document\Company $company)
    {
        $this->company = $company;
        return $this;
    }

    /**
     * Get company
     *
     * @return AppBundle\Document\Company $company
     */
    public function getCompany()
    {
        return $this->company;
    }
}
