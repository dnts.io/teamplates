<?php

namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document
 * @MongoDB\InheritanceType("SINGLE_COLLECTION")
 * @MongoDB\DiscriminatorField("fieldType")
 * @MongoDB\DiscriminatorMap({
 *     "text"="AppBundle\Document\Field\Text",
 *     "numeric"="AppBundle\Document\Field\FNumeric",
 *     "age"="AppBundle\Document\Field\Age",
 *     "boolean"="AppBundle\Document\Field\FBoolean",
 *     "group"="AppBundle\Document\Field\GroupField"
 * })
 */
class Field
{
    /**
     * @MongoDB\Id
     */
    protected $id;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $name;

    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $isLimitedInTime;

    /**
     * @MongoDB\Field(type="date")
     */
    protected $limitedDateFrom;

    /**
     * @MongoDB\Field(type="date")
     */
    protected $limitedDateTo;

    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $isRequired;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $defaultValue;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $publicFieldType;

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set isLimitedInTime
     *
     * @param boolean $isLimitedInTime
     *
     * @return $this
     */
    public function setIsLimitedInTime($isLimitedInTime)
    {
        $this->isLimitedInTime = $isLimitedInTime;

        return $this;
    }

    /**
     * Get isLimitedInTime
     *
     * @return boolean $isLimitedInTime
     */
    public function getIsLimitedInTime()
    {
        return $this->isLimitedInTime;
    }

    /**
     * Set limitedDateFrom
     *
     * @param \DateTime $limitedDateFrom
     *
     * @return $this
     */
    public function setLimitedDateFrom($limitedDateFrom)
    {
        $this->limitedDateFrom = $limitedDateFrom;

        return $this;
    }

    /**
     * Get limitedDateFrom
     *
     * @return \DateTime $limitedDateFrom
     */
    public function getLimitedDateFrom()
    {
        return $this->limitedDateFrom;
    }

    /**
     * Set limitedDateTo
     *
     * @param \DateTime $limitedDateTo
     *
     * @return $this
     */
    public function setLimitedDateTo($limitedDateTo)
    {
        $this->limitedDateTo = $limitedDateTo;

        return $this;
    }

    /**
     * Get limitedDateTo
     *
     * @return \DateTime $limitedDateTo
     */
    public function getLimitedDateTo()
    {
        return $this->limitedDateTo;
    }

    /**
     * Set isRequired
     *
     * @param boolean $isRequired
     *
     * @return $this
     */
    public function setIsRequired($isRequired)
    {
        $this->isRequired = $isRequired;

        return $this;
    }

    /**
     * Get isRequired
     *
     * @return boolean $isRequired
     */
    public function getIsRequired()
    {
        return $this->isRequired;
    }

    /**
     * Set defaultValue
     *
     * @param string $defaultValue
     *
     * @return $this
     */
    public function setDefaultValue($defaultValue)
    {
        $this->defaultValue = $defaultValue;

        return $this;
    }

    /**
     * Get defaultValue
     *
     * @return string $defaultValue
     */
    public function getDefaultValue()
    {
        return $this->defaultValue;
    }

    /**
     * Set publicFieldType
     *
     * @param string $publicFieldType
     *
     * @return $this
     */
    public function setPublicFieldType($publicFieldType)
    {
        $this->publicFieldType = $publicFieldType;

        return $this;
    }

    /**
     * Get publicFieldType
     *
     * @return string $publicFieldType
     */
    public function getPublicFieldType()
    {
        return $this->publicFieldType;
    }
}
