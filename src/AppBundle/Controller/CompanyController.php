<?php

namespace AppBundle\Controller;

use AppBundle\Document\Company;
use AppBundle\Form\CompanyCreateType;
use AppBundle\Form\CompanyType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CompanyController extends Controller
{

    /**
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function indexAction(Request $request)
    {
        $dm        = $this->get('doctrine_mongodb')->getManager();
        $companies = $this->get('company_repository')->findAll();

        $company = new Company();
        $form    = $this->createForm(CompanyCreateType::class, $company);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $dm->persist($company);
            $dm->flush();

            return $this->redirectToRoute('companies_index');
        }

        return $this->render(
            '@App/company/index.html.twig',
            ['companies' => $companies, 'form' => $form->createView()]
        );
    }

    /**
     * @param string  $companyId
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function editAction($companyId, Request $request)
    {
        $dm      = $this->get('doctrine_mongodb')->getManager();
        $company = $this->get('app_get_by_id')->getCompany($companyId);

        $form = $this->createForm(CompanyType::class, $company);

        $oldImageName = $company->getLogo();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $file = $company->getLogo();

            if ($file) {
                // Generate a unique name for the file before saving it
                $fileName = sha1(uniqid()).'.'.$file->guessExtension();

                // Move the file to the directory where brochures are stored
                $file->move(
                    $this->getParameter('dir_companies_absolute'),
                    $fileName
                );

                $company->setLogo($fileName);
            } else {
                $company->setLogo($oldImageName);
            }

            $dm->flush();

            return $this->redirectToRoute('companies_index');
        }

        return $this->render(
            '@App/company/edit.html.twig',
            ['form' => $form->createView(), 'company' => $company]
        );
    }
}
