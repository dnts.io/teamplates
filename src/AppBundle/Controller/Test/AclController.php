<?php

namespace AppBundle\Controller\Test;

use AppBundle\Document\AclRule;
use AppBundle\Document\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class AclController extends Controller
{

    /**
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function indexAction(Request $request)
    {


        $dm = $this->get('doctrine_mongodb')->getManager();
        //$node = $dm->getRepository('AppBundle:Node')->find($nodeId);

        $aclRole = $this->get('acl_role');

        /** @var User $user */
        $user  = $dm->getRepository('AppBundle:User')->find('57bab343f893025ad445c6bb'); // pom2
        $group = $dm->getRepository('AppBundle:Group')->find('57b607e9f893021b8312ffd1'); // financial


        $roles = [$aclRole::ROLE_OWNER, $aclRole::ROLE_WRITER];

        $aclRule = $dm->getRepository('AppBundle:AclRule')->findByGroupAndUser($group, $user);

        if (!$aclRule) {
            $aclRule = new AclRule();
            $aclRule->setUser($user)->setGroup($group);
        }
        $aclRule->setRoles($roles);
        $dm->persist($aclRule);
        $dm->flush();
        /**/

        var_dump(
            $dm->getRepository('AppBundle:AclRule')->findByGroupAndRole($group, $aclRole::ROLE_WRITER)
        );

        return $this->render('AppBundle:test/acl:index.html.twig', []);
    }
}
