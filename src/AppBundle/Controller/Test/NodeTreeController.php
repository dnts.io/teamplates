<?php

namespace AppBundle\Controller\Test;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class NodeTreeController extends Controller
{

    /**
     * @param string $nodeId
     *
     * @return Response
     */
    public function indexAction($nodeId)
    {
        $node = $this->get('node_repository')->find($nodeId);
        $tree = $this->get('node_structure')->getTreeNodes($node);

        echo '<ol>';
        $this->showTree($tree);
        echo '</ol>';

        return $this->render('AppBundle:test:nothing.html.twig', []);
    }

    /**
     * @param array $tree
     */
    protected function showTree(array $tree)
    {
        printf(
            '<li><i>%s</i> - <b>%s</b> - %s/%s</li>',
            $tree['id'],
            $tree['title'],
            $tree['path'],
            $tree['level']
        );

        if (isset($tree['children'])) {
            echo '<ol>';
            foreach ($tree['children'] as $child) {
                $this->showTree($child);
            }
            echo '</ol>';
        }
    }
}
