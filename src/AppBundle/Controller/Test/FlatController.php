<?php

namespace AppBundle\Controller\Test;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class FlatController extends Controller
{

    /**
     * @param string  $nodeId
     *
     * @return JsonResponse
     */
    public function indexAction($nodeId)
    {
        $node = $this->get('node_repository')->find($nodeId);
        $flatten = $this->get('node_structure')->getFlattenNodes($node);

        var_dump($flatten);

        return $this->render('AppBundle:test:nothing.html.twig', []);
    }
}
