<?php

namespace AppBundle\Controller\Test;

use AppBundle\Form\MailType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class MailController extends Controller
{

    /**
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function sendAction(Request $request)
    {
        $form = $this->createForm(MailType::class);

        $form->handleRequest($request);
        $this->addFlash('notice', 'Hello !');

        if ($form->isSubmitted() && $form->isValid()) {

            $mailer = $this->get('mailer');

            $message = new \Swift_Message(
                $form->get('subject')->getData(),
                $form->get('body')->getData()
            );
            $message->setFrom($form->get('from')->getData());
            $message->setTo($form->get('to')->getData());

            if ($mailer->send($message)) {
                $this->addFlash('notice', 'Message sent !');
            } else {
                $this->addFlash('error', ':(');
            }

            return $this->redirect($this->generateUrl('test_mail_send'));
        }

        return $this->render('AppBundle:mail:form.html.twig', ['form' => $form->createView()]);
    }
}
