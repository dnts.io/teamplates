<?php

namespace AppBundle\Controller;

use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class CkEditorController extends Controller
{

    /**
     * @param string  $templateId
     * @param Request $request
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function fileBrowserImageDropAction($templateId, Request $request)
    {
        $fileBag = $request->files->all();
        if (!is_array($fileBag) || !isset($fileBag['upload'])) {
            throw new Exception();
        }

        /** @var UploadedFile $uploadedFile */
        $uploadedFile = $fileBag['upload'];

        $newFile = new File(
            $this->get('file_upload')->getPath($uploadedFile),
            false
        );

        $newFolder = $newFile->getPathInfo();

        /*
         * Create directory "uploads" if it needs
         */
        if (!$newFolder->isDir()) {
            mkdir($newFolder->getPathname());
        }

        /*
         * The filename is created with the content hash,
         * So, if the file exists, the file content is same and we don't copy it and delete it
         */
        if (!$newFile->isFile()) {
            $uploadedFile->move($newFolder->getRealPath(), $newFile->getBasename());
        } else {
            unlink($uploadedFile->getPathname());
        }

        return $this->json(
            [
                "uploaded" => 1,
                "fileName" => $newFile->getBasename(),
                "url"      => $this->get('file_upload')->getUrl($uploadedFile),
            ]
        );
    }
}
