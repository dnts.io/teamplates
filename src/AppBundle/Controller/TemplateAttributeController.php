<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TemplateAttributeController extends Controller
{

    public function searchAction(Request $request)
    {
        $name                  = $request->get('q');
        $attributeDocumentName = $this->get('template_attribute_document_guesser')->getDocumentName();
        $result                = $this->get('doctrine_mongodb')
                                      ->getManager()
                                      ->getRepository('AppBundle:'.$attributeDocumentName)->searchByName($name);

        $rs = [];
        foreach ($result->execute() as $r) {
            $rs[] = [
                'id'   => $r->getId(),
                'text' => $r->getName(),
            ];
        }

        return $this->json($rs);
    }


    public function formAction()
    {
        $formClass = $this->get('template_attribute_form_guesser')->getFormClass();
        $form      = $this->createForm($formClass);

        return $this->render('AppBundle:template-attribute:form.html.twig', ['form' => $form->createView()]);
    }

    public function addAction($type, Request $request)
    {
        $attributeDocument = $this->get('template_attribute_document_guesser')->getDocument();
        $attributeForm     = $this->get('template_attribute_form_guesser')->getFormClass();

        $form = $this->createForm($attributeForm, $attributeDocument);

        $form->handleRequest($request);

        if ($form->isValid() && $request->isXmlHttpRequest()) {
            try {
                $dm = $this->get('doctrine_mongodb')->getManager();
                $dm->persist($attributeDocument);
                $dm->flush();

                $data = ['id' => $attributeDocument->getId(), 'text' => $attributeDocument->getName()];
            } catch (\MongoDuplicateKeyException $e) {
                $data = ['id' => $attributeDocument->getId(), 'text' => $attributeDocument->getName()];
            }
            return $this->json(['success' => 1, 'data' => $data]);
        }

        return $this->json(['success' => 0], Response::HTTP_NOT_ACCEPTABLE);
    }
}
