<?php

namespace AppBundle\Controller;

use AppBundle\Document\Node;
use AppBundle\Form\NodeType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class NodeController extends Controller
{

    /**
     * @param string $templateId
     *
     * @return JsonResponse
     */
    public function getNodeByTemplateAction($templateId)
    {
        $template = $this->get('app_get_by_id')->getTemplate($templateId);

        $nodeRepository = $this->get('node_repository');
        $nodeRepository->setChildrenIndex('children');

        $nodes = $nodeRepository->childrenHierarchy($template->getAlphaNode());

        $nodes = $this->replaceArray((array) $nodes);

        return $this->json($nodes);
    }

    /**
     * @param array $nodes
     *
     * @return array
     * @todo MOVE THIS FROM HERE
     */
    private function replaceArray(array $nodes)
    {
        $nodesKeys = array_keys($nodes);
        foreach ($nodesKeys as $key) {
            unset($nodes[$key]['path']);
            unset($nodes[$key]['level']);
            unset($nodes[$key]['content']);
            unset($nodes[$key]['parent']);


            $nodes[$key]['_id']       = (string) $nodes[$key]['_id'];
            $nodes[$key]['isEditing'] = false;
            if (isset($nodes[$key]['editing'])) {
                $lock                             = $this->get('node_editing')->setFromMongoDB($nodes[$key]['lock']);
                $nodes[$key]['isEditing']         = $lock->isEditingByAnotherOne();
                $nodes[$key]['lock']['username']  = $lock->getUser()->getUsername();
                $nodes[$key]['lock']['startDate'] = $lock->getStartDate()->format(DATE_W3C);
            }

            if ($nodes[$key]['children']) {

                $nodes[$key]['children'] = $this->replaceArray($nodes[$key]['children']);
            } else {
                unset($nodes[$key]['children']);
            }
        }

        return $nodes;
    }

    /**
     * @param $nodeId
     *
     * @return Response
     */
    public function getContentAction($nodeId)
    {
        $node = $this->get('app_get_by_id')->getNode($nodeId);

        $this->startNodeEdition($node);

        $nodeForm = $this->createForm(NodeType::class, $node);

        $this->get('node_logger')->logInfo($node->getId(), 'node_content_edit', 'get', $node);

        return $this->render('AppBundle:node:node.html.twig', ['form' => $nodeForm->createView(), 'nodeId' => $nodeId]);
    }

    /**
     * @param Node $node
     */
    protected function startNodeEdition(Node $node)
    {
        $dm = $this->get('doctrine_mongodb')->getManager();
        $node->setLock($this->get('node_editing')->toMongoDB());
        $dm->flush();

        $this->get('node_logger')->logInfo($node->getId(), 'node_edition', 'start', $node);

    }

    /**
     * @param int     $nodeId
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function saveContentAction($nodeId, Request $request)
    {
        $dm   = $this->get('doctrine_mongodb')->getManager();
        $node = $this->get('app_get_by_id')->getNode($nodeId);

        if (!$node) {
            return $this->json(['success' => 0, 'error' => 'No node found for id '.$nodeId]);
        }

        $response = $this->checkIfIsEditing($node);
        if ($response) {
            return $response;
        }

        // Reset the lock
        $this->startNodeEdition($node);

        $content = $this->get('node_content_cleaner')->clean($request->get('content'));

        $node->setContent($content);
        $dm->flush();

        $this->get('node_logger')->logInfo($node->getId(), 'node_content_edit', 'saved', $node);

        return $this->json(['success' => 1]);
    }

    /**
     * @param string $nodeId
     *
     * @return JsonResponse
     */
    public function stopEditionAction($nodeId)
    {
        $node = $this->get('app_get_by_id')->getNode($nodeId);

        $nodeEdition = $this->get('node_editing')->setFromMongoDB($node->getLock());

        if ($nodeEdition !== null) {
            if ($nodeEdition->isEditingByAnotherOne()) {
                return $this->json(
                    [
                        'success' => 0,
                        'error'   => sprintf(
                            'The node %s is editing by %s',
                            $nodeId,
                            $nodeEdition->getUser()->getId()
                        ),
                    ]
                );
            }
            $this->stopNodeEdition($node);
        }

        return $this->json(['success' => 1]);
    }

    /**
     * @param Node $node
     */
    protected function stopNodeEdition(Node $node)
    {
        $dm = $this->get('doctrine_mongodb')->getManager();
        $node->setLock(null);
        $dm->flush();

        $this->get('node_logger')->logInfo($node->getId(), 'node_edition', 'stopped', $node);
    }

    /**
     * @param string  $parentId
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function createAction($parentId, Request $request)
    {
        $dm       = $this->get('doctrine_mongodb')->getManager();
        $template = $this->get('app_get_by_id')->getTemplate($parentId);

        $childCount = $this->get('node_repository')->childCount($template->getAlphaNode(), true);

        $node = new Node();
        $node->setTitle($request->get('title'));
        $node->setOrder($childCount + 1);
        $node->setParent($template->getAlphaNode());
        $dm->persist($node);

        $dm->flush();
        $this->get('node_logger')->logInfo($node->getId(), 'node_creation', 'saved', $node);
        $this->get('template_logger')->logInfo(
            $template->getId(),
            'template_node_add',
            'saved',
            $template
        );

        return $this->json(['_id' => $node->getId(), 'title' => $node->getTitle()]);
    }

    /**
     * @param string $parentId
     *
     * @return JsonResponse
     */
    public function addSubNodeAction($parentId)
    {
        $nodeRepository = $this->get('node_repository');
        /** @var Node $parentNode */
        $parentNode = $this->get('app_get_by_id')->getNode($parentId);

        $childCount = $nodeRepository->childCount($parentNode, true);

        $node = new Node();
        $node->setTitle('Title '.($parentNode->getLevel()));
        $node->setParent($parentNode);
        $node->setOrder($childCount + 1);
        $dm = $this->get('doctrine_mongodb')->getManager();
        $dm->persist($node);
        $dm->flush();

        $this->get('node_logger')->logInfo($node->getId(), 'node_child_add', 'saved');

        return $this->json(['_id' => $node->getId(), 'title' => $node->getTitle()]);
    }

    /**
     * @param string  $nodeId
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function saveTitleNodeAction($nodeId, Request $request)
    {
        $dm   = $this->get('doctrine_mongodb')->getManager();
        $node = $this->get('app_get_by_id')->getNode($nodeId);

        $title = $request->get('title');

        $node->setTitle($title);
        $dm->flush();

        $this->get('node_logger')->logInfo($node->getId(), 'node_title_edit', 'saved', $node);

        return $this->json(['success' => 1]);
    }

    /**
     * @param string $nodeId
     *
     * @return Response
     */
    public function viewContentAction($nodeId)
    {
        $node = $this->get('app_get_by_id')->getNode($nodeId);

        return $this->get('node_html_render')->getNodeRenderResponse($node);
    }


    /**
     * @param string $nodeId
     *
     * @return Response
     */
    public function editContentAction($nodeId)
    {
        $node = $this->get('app_get_by_id')->getNode($nodeId);

        $response = $this->checkIfIsEditing($node);
        if ($response) {
            return $response;
        }

        $this->startNodeEdition($node);

        $nodeForm = $this->createForm(NodeType::class, $node);

        $this->get('node_logger')->logInfo($node->getId(), 'node_content_edit', 'get', $node);

        return $this->render('@App/node/edit.html.twig', ['form' => $nodeForm->createView()]);
    }

    /**
     * @param Node $node
     *
     * @return bool|JsonResponse
     */
    protected function checkIfIsEditing(Node $node)
    {
        $lock = $this->get('node_editing')->setFromMongoDB($node->getLock());
        if ($lock->isEditingByAnotherOne()) {
            return $this->json(
                [
                    'success' => 0,
                    'error'   => sprintf(
                        'Node is editing by another user (%s) since %s',
                        $lock->getUser(),
                        $lock->getStartDate()->format(DATE_W3C)
                    ),
                ],
                403
            );
        }

        return false;
    }

    /**
     * @param string $nodeId
     *
     * @return JsonResponse
     */
    public function validateAction($nodeId)
    {
        return $this->validate($nodeId, true);
    }

    /**
     * @param string $nodeId
     *
     * @return JsonResponse
     */
    public function invalidateAction($nodeId)
    {
        return $this->validate($nodeId, false);
    }

    /**
     * @param string $nodeId
     * @param bool   $validated
     *
     * @return JsonResponse
     */
    public function validate($nodeId, $validated)
    {
        $node = $this->get('app_get_by_id')->getNode($nodeId);
        $node->setValidated($validated);

        $this->get('node_logger')->logInfo($nodeId, 'node_validation', $validated ? 'start' : 'stopped');

        $this->get('doctrine_mongodb')->getManager()->flush();

        return $this->json(
            [
                'success'       => 1,
                'nodeId'        => $node->getId(),
                'nodeValidated' => $node->getValidated(),
            ]
        );
    }

    /**
     * @param string $nodeId
     *
     * @return JsonResponse|RedirectResponse
     */
    public function removeAction($nodeId)
    {
        $node = $this->get('app_get_by_id')->getNode($nodeId);

        // We remove the parent
        $node->setParent(null);

        $this->get('node_logger')->logInfo($nodeId, 'node_remove', 'before', $node);
        $this->get('node_logger')->logInfo($nodeId, 'node_remove', 'after', $node);

        $this->get('doctrine_mongodb')->getManager()->flush();

        return $this->json(
            [
                'success' => 1,
                'nodeId'  => $node->getId(),
                'status'  => 'removed',
            ]
        );
    }
}
