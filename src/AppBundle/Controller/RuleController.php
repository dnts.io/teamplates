<?php

namespace AppBundle\Controller;

use AppBundle\Document\Field;
use AppBundle\Document\Node;
use AppBundle\Document\NodeRule;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class RuleController extends Controller
{

    /**
     * @param $nodeId
     *
     * @return JsonResponse
     */
    public function createAction($nodeId)
    {
        $node = $this->get('app_get_by_id')->getNode($nodeId);

        $dm = $this->get('doctrine_mongodb')->getManager();

        // We remove previous rules attached to this node
        foreach ($node->getRules() as $rule) {
            $node->removeRule($rule);
            $dm->remove($rule);
        }

        $rulesService = $this->get('rule_from_form');

        $node->setRuleAction($rulesService->getRulesAction());

        if ($validity = $rulesService->getRulesValidity()) {
            $node->setRuleValidityFrom(\DateTime::createFromFormat('d/m/Y', $validity['from']));
            $node->setRuleValidityTo(\DateTime::createFromFormat('d/m/Y', $validity['to']));
        }

        foreach ($rulesService->getRules() as $rule) {
            $field        = $dm->getRepository(Field::class)->find($rule['field']);
            $ruleDocument = new NodeRule();
            $ruleDocument->setField($field);
            $ruleDocument->setLogic($rule['field-logic']);
            $ruleDocument->setRule($rule['field-condition']);
            $ruleDocument->setValue($rule['field-value-condition']);

            $dm->persist($ruleDocument);
            $node->addRule($ruleDocument);
        }
        $dm->flush();

        return $this->json(['success' => 1]);
    }


    /**
     * @param $nodeId
     *
     * @return JsonResponse
     */
    public function removeAllAction($nodeId)
    {
        $node = $this->get('app_get_by_id')->getNode($nodeId);

        $dm = $this->get('doctrine_mongodb')->getManager();

        foreach ($node->getRules() as $rule) {
            $node->removeRule($rule);
            $dm->remove($rule);
        }
        $dm->flush();

        return $this->json(['success' => 1]);
    }
}
