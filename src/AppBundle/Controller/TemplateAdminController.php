<?php

namespace AppBundle\Controller;

use AppBundle\Document\Template;
use AppBundle\Form\TemplateCompanyType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TemplateAdminController extends Controller
{

    /**
     * @Security("has_role('ROLE_ADMIN')")
     *
     * @return Response
     */
    public function indexAction()
    {
        /** @var Template $templates */
        $templates = $this->get('template_repository')->findBy(['status' => Template::STATUS_VALIDATED]);

        return $this->render('@App/template-admin/index.html.twig', ['templates' => $templates]);
    }

    public function editAction($templateId, Request $request)
    {
        $template = $this->get('app_get_by_id')->getTemplate($templateId);

        $form = $this->createForm(TemplateCompanyType::class, $template);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->get('doctrine_mongodb')->getManager()->flush();

            return $this->redirectToRoute('template_company_index');
        }

        return $this->render(
            '@App/template-admin/edit.html.twig',
            ['template' => $template, 'form' => $form->createView()]
        );
    }
}
