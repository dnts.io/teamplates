<?php

namespace AppBundle\Controller;

use AppBundle\Document\Field;
use AppBundle\Document\Field\GroupField;
use AppBundle\Document\Filling;
use AppBundle\Document\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class FieldController
 * @package AppBundle\Controller
 *
 * @method User getUser
 */
class FieldController extends Controller
{

    /**
     * This controller is called when a user create a field (we have to check if values are "ok")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function createAction(Request $request)
    {
        $dm       = $this->get('doctrine_mongodb')->getManager();
        $template = $this->get('app_get_by_id')->getTemplate($request->get('templateId'));

        $this->get('app_input_checker')->checkTemplate($template);

        $fieldValidator = $this->get('field_validator_factory')->create();

        if ($fieldValidator->isValid()) {

            $fieldDocument = $this->get('field_document_factory')->create();
            $fieldDocument->convert();
            $document = $fieldDocument->getDocument();

            $dm->persist($document);
            $template->addField($document);
            $dm->flush();

            return $this->json(['success' => 1, 'fieldId' => $document->getId(), 'fieldName' => $document->getName()]);
        }

        return $this->json(['success' => 0, 'errors' => $fieldValidator->getErrors()]);
    }

    public function createGroupAction(Request $request)
    {
        $dm       = $this->get('doctrine_mongodb')->getManager();
        $template = $this->get('app_get_by_id')->getTemplate($request->get('templateId'));

        $this->get('app_input_checker')->checkTemplate($template);

        $fieldGroup = $this->get('field_group_subscribers');

        $group  = $fieldGroup->createGroup();
        $fields = $fieldGroup->createFields();

        $dm->persist($group);
        foreach ($fields as $field) {
            $dm->persist($field);
        }

        $template->addField($group);
        $dm->flush();

        return $this->json(['success' => 1, 'groupId' => $group->getId()]);
    }


    public function getGroupFieldsAction($groupFieldId)
    {
        $groupField = $this->get('app_get_by_id')->getGroupField($groupFieldId);

        $fields = ['groupField' => ['id' => $groupFieldId], 'fields' => []];
        /** @var Field $field */
        foreach ($groupField->getFields() as $field) {
            $fields['fields'][$field->getName()] = [
                'id'   => $field->getId(),
                'name' => $field->getName(),
            ];
        }

        return $this->json($fields);
    }

    public function newGroupFieldItemAction($groupFieldId, $fillingId)
    {
        $em = $this->get('doctrine_mongodb')->getManager();

        $filling = $this->get('app_get_by_id')->getFilling($fillingId);
        $group   = $this->get('app_get_by_id')->getGroupField($groupFieldId);

        $this->get('app_input_checker')->checkFilling($filling);

        $values = $filling->getGroupValue();
        if (!isset($values[$groupFieldId])) {
            $values[$groupFieldId] = [];
        }
        $currentGroupValue = $values[$groupFieldId];
        $nbItems           = count($currentGroupValue);
        $currentItemIndex  = $nbItems + 1;

        $newGroupFieldValue = $values;

        $fieldsArray = [];
        /** @var Field $field */
        foreach ($group->getFields() as $k => $field) {
            $fieldsArray[] = [
                'id'    => $field->getId(),
                'name'  => $field->getName().'_'.($currentItemIndex),
                'value' => '',
            ];
        }
        $newGroupFieldValue[$groupFieldId][$currentItemIndex] = $fieldsArray;
        $values[$groupFieldId]                                = $newGroupFieldValue[$groupFieldId];

        $filling->setGroupValue($values);
        $em->flush();

        return $this->json(
            [
                'success' => 1,
                'item'    => $this->renderView(
                    "@App/field/group-item.html.twig",
                    [
                        'fields'           => $fieldsArray,
                        'groupId'          => $groupFieldId,
                        'currentItemIndex' => $currentItemIndex,
                    ]
                ),
            ]
        );
    }

    /**
     * @param $fieldId
     *
     * @return JsonResponse
     */
    public function validateAction($fieldId)
    {
        $field = $this->get('app_get_by_id')->getField($fieldId);

        // TODO : error if no field

        $checker = $this->get('field_checker')->create($field);

        return $this->json(['valid' => $checker->isValid(), 'errors' => $checker->getErrors()]);
    }
}
