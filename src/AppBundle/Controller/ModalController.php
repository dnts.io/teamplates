<?php

namespace AppBundle\Controller;

use AppBundle\Document\Node;
use AppBundle\Document\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ModalController extends Controller
{
    public function fieldAction($templateId)
    {
        $template = $this->get('app_get_by_id')->getTemplate($templateId);

        // @TODO preload data when editing !
        return $this->render('AppBundle:modal:field.html.twig', ['fields' => $template->getFields()]);
    }

    public function nodeAccessAction($templateId, $nodeId)
    {
        $template = $this->get('app_get_by_id')->getTemplate($templateId);
        $node     = $this->get('app_get_by_id')->getNode($nodeId);

        $fields = $template->getFields();

        return $this->render(
            'AppBundle:modal:node-access.html.twig',
            ['fields' => $fields, 'node' => $node, 'rules' => $node->getRules()]
        );
    }
}
