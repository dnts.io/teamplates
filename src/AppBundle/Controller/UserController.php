<?php

namespace AppBundle\Controller;

use AppBundle\Form\UserCompanyType;
use AppBundle\Form\UserRoleType;
use Doctrine\ODM\MongoDB\DocumentRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{

    /**
     * @Security("has_role('ROLE_ADMIN')")
     *
     * @return Response
     */
    public function indexAction()
    {
        $users = $this->getUserRepository()->findAll();

        return $this->render(
            '@App/user/index.html.twig',
            ['users' => $users]
        );
    }


    /**
     * @Security("has_role('ROLE_ADMIN')")
     *
     * @param         $userId
     * @param Request $request
     *
     * @return Response
     */
    public function editRolesAction($userId, Request $request)
    {
        $dm   = $this->get('doctrine_mongodb')->getManager();
        $user = $this->getUserRepository()->find($userId);

        $form = $this->createForm(UserRoleType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $dm->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render(
            '@App/user/edit-roles.html.twig',
            ['user' => $user, 'form' => $form->createView()]
        );
    }

    public function editCompanyAction($userId, Request $request)
    {
        $dm   = $this->get('doctrine_mongodb')->getManager();
        $user = $this->getUserRepository()->find($userId);

        $form = $this->createForm(UserCompanyType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $dm->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render(
            '@App/user/edit-company.html.twig',
            ['user' => $user, 'form' => $form->createView()]
        );
    }

    /**
     * @param string $userId
     * @param bool   $isEnable
     *
     * @return RedirectResponse
     */
    public function editEnableAction($userId, $isEnable)
    {
        $dm   = $this->get('doctrine_mongodb')->getManager();
        $user = $this->getUserRepository()->find($userId);

        $user->setEnabled($isEnable);
        $dm->persist($user);
        $dm->flush();

        return $this->redirectToRoute('user_index');
    }

    /**
     * @return DocumentRepository
     */
    private function getUserRepository()
    {
        return $this->get('user_repository');
    }
}
