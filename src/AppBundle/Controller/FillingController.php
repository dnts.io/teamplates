<?php

namespace AppBundle\Controller;

use AppBundle\Document\Filling;
use AppBundle\Document\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class FillingController extends Controller
{

    public function createAction($templateId, Request $request)
    {
        $internalRef = trim($request->get('internalRef'));

        if (!$internalRef) {
            $this->addFlash('error', 'Please give use an internal ref');

            return $this->redirectToRoute('homepage');
        }

        $dm = $this->get('doctrine_mongodb')->getManager();

        $template = $this->get('app_get_by_id')->getTemplate($templateId);

        $fillingDocument = new Filling();
        $fillingDocument->setInternalRef($internalRef);
        $fillingDocument->setCreationDate(new \DateTime());
        $fillingDocument->setUpdateDate(new \DateTime());
        $fillingDocument->setFiller($this->getUser());
        $fillingDocument->setTemplate($template);

        $dm->persist($fillingDocument);
        $dm->flush();

        return $this->redirectToRoute('filling_edit', ['fillingId' => $fillingDocument->getId()]);
    }

    public function editAction($fillingId, $pageNb)
    {
        $filling = $this->getFilling($fillingId);

        if ($this->get('filling_lock')->isLock($filling)) {
            $this->addFlash('error', 'This node is currently locked');

            return $this->redirectToRoute('homepage');
        }
        $dm = $this->get('doctrine_mongodb')->getManager();

        // Lock the current document
        $filling->setStatus(Filling::STATUS_LOCK);
        $dm->flush();

        $template = $filling->getTemplate();

        $flatten = $this->get('node_structure')->getFlattenNodes($template->getAlphaNode());
        array_shift($flatten);

        $currentNodeId = $flatten[$pageNb - 1];

        return $this->render(
            'AppBundle:filling:fill.html.twig',
            [
                'filling'       => $filling,
                'template'      => $template,
                'nbNode'        => count($flatten),
                'nbField'       => count($template->getFields()),
                'currentNodeId' => $currentNodeId,
                'nodeIds'       => $flatten,
            ]
        );
    }

    public function removeAction($fillingId)
    {
        $filling = $this->getFilling($fillingId);

        $dm          = $this->get('doctrine_mongodb')->getManager();
        $internalRef = $filling->getInternalRef();
        $dm->remove($filling);
        $dm->flush();

        $this->addFlash('info', 'Le document avec la référence "'.$internalRef.'" a été supprimée');

        return $this->redirectToRoute('homepage');
    }

    public function quitAction($fillingId)
    {
        $filling = $this->getFilling($fillingId);

        $dm = $this->get('doctrine_mongodb')->getManager();
        $filling->setStatus(Filling::STATUS_UNLOCK);
        $dm->flush();

        return $this->redirectToRoute('homepage');
    }

    public function saveAction($fillingId)
    {
        $filling = $this->getFilling($fillingId);

        $dm = $this->get('doctrine_mongodb')->getManager();

        $filling->setValues($this->get('field_form')->get('values'));


        // TODO !! We will have to put all of this in a service :

        $groupValues = $this->get('field_form')->get('groupValues');
        if ($groupValues) {
            $arrayToSave = [];
            foreach ($groupValues as $groupId => $items) {
                $arrayToSave[$groupId] = [];

                foreach ($items as $indexItem => $fields) {
                    $arrayToSave[$groupId][$indexItem] = [];
                    foreach ($fields as $fieldKey => $fieldValue) {
                        // we explode on underscore because of
                        // formatting it in the view see 'group-item.html.twig'
                        $explodedArray = explode('_', $fieldKey);
                        if (isset($explodedArray[2])) {
                            list($name, $itemKey, $id) = $explodedArray;
                            $arrayToSave[$groupId][$indexItem][] = [
                                'name'  => $name.'_'.$itemKey,
                                'id'    => $id,
                                'value' => $fieldValue,
                            ];
                        }

                    }
                }
            }
            $filling->setGroupValue($arrayToSave);
        }
        $filling->setUpdateDate(new \DateTime());

        $dm->flush();

        return $this->json(['success' => 1]);
    }

    /**
     * @param string $fillingId
     * @param string $nodeId
     *
     * @return Response
     */
    public function viewAction($fillingId, $nodeId)
    {
        $filling = $this->getFilling($fillingId);
        $node    = $this->get('app_get_by_id')->getNode($nodeId);

        return $this->get('node_html_render_values')->getNodeRenderResponse(
            $node,
            $filling
        );
    }


    public function viewNodeOnlyAction($fillingId, $nodeId)
    {
        $filling = $this->getFilling($fillingId);
        $node    = $this->get('app_get_by_id')->getNode($nodeId);

        return $this->get('node_html_render_values')->getNodeRenderResponse(
            $node,
            $filling,
            false // we don't display child nodes
        );
    }


    /**
     * @param string $fillingId
     * @param string $format
     *
     * @return BinaryFileResponse|RedirectResponse
     */
    public function printAction($fillingId, $format)
    {
        $filling  = $this->getFilling($fillingId);
        $template = $filling->getTemplate();

        return $this->get('html_converter')->renderAsResponse(
            $this->get('node_html_render_values')->getNodeRenderResponse(
                $template->getAlphaNode(),
                $filling
            )->getContent(),
            $format,
            (string) $filling->getId().'.'.$format
        );
    }

    /**
     * @param string $fillingId
     *
     * @return JsonResponse
     */
    public function fillingStatsAction($fillingId)
    {
        $filling     = $this->getFilling($fillingId);
        $totalFields = $filling->getTemplate()->getFields()->count();

        $fieldFilled = 0;
        foreach ($filling->getValues() as $value) {
            if (!empty($value)) {
                $fieldFilled++;
            }
        }

        if ($filling->getGroupValue()) {

            foreach ($filling->getGroupValue() as $groupKey => $groupValues) {

                $firstGroupField = array_shift($groupValues);

                $totalGroupField    = count($firstGroupField);
                $totalOfFieldFilled = 0;
                foreach ($firstGroupField as $field) {
                    if (!empty($field['value'])) {
                        $totalOfFieldFilled++;
                    }
                }
                if ($totalGroupField === $totalOfFieldFilled) {
                    $fieldFilled++;
                }
            }
        }

        $percent = $fieldFilled / $totalFields * 100;

        return $this->json(['totalFields' => $totalFields, 'filledField' => $fieldFilled, 'percent' => $percent]);
    }

    /**
     * @param string $fillingId
     *
     * @return Filling
     */
    protected function getFilling($fillingId)
    {
        return $this->get('app_get_by_id')->getFilling($fillingId);
    }
}
