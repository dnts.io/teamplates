<?php

namespace AppBundle\Controller;

use AppBundle\Document\Node;
use AppBundle\Document\Template;
use AppBundle\Form\TemplateType;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TemplateController extends Controller
{

    /**
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function createAction(Request $request)
    {
        $template = new Template();
        $form     = $this->createForm(TemplateType::class, $template);

        $form->handleRequest($request);
        $logger = $this->get('template_logger');

        if ($form->isSubmitted() && $form->isValid()) {

            $alphaNode = new Node();

            $templateForm = $form->getData();
            $template->setOwner($this->getUser());
            $template->setAlphaNode($alphaNode);
            $template->setCreationDate(new \DateTime());
            $template->setStatus(Template::STATUS_EDITABLE);

            /** @var ObjectManager $dm */
            $dm = $this->get('doctrine_mongodb')->getManager();
            $dm->persist($alphaNode);
            $dm->persist($templateForm);
            $dm->flush();

            $logger->logInfo($templateForm->getId(), 'template_creation', 'saved', $form->getData());

            return $this->redirect($this->generateUrl('template_edit', ['id' => $template->getId()]));
        }

        $logger->logInfo(0, 'template_creation', 'started', $form->getData());

        return $this->render('AppBundle:template:form.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @param string  $id
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function editAction($id, Request $request)
    {
        $logger = $this->get('template_logger');

        $template = $this->getTemplate($id);

        $form = $this->createForm(TemplateType::class, $template);

        $data = $form->getData();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $logger->logInfo($id, 'template_edition', 'saved', $data);

            $dm = $this->get('doctrine_mongodb')->getManager();
            $dm->flush();

            return $this->redirect($this->generateUrl('template_edit', ['id' => $template->getId()]));
        }

        $nodes = $this->get('node_repository')->childrenHierarchy($template->getAlphaNode());

        $logger->logInfo($id, 'template_edition', 'start', $data);

        return $this->render(
            'AppBundle:template:form.html.twig',
            ['form' => $form->createView(), 'template' => $template, 'nodes' => $nodes]
        );
    }

    public function restaureEditingAction($templateId)
    {
        $template = $this->getTemplate($templateId);

        $template->setStatus(Template::STATUS_EDITABLE);
        $this->get('doctrine_mongodb')->getManager()->flush();

        return $this->redirectToRoute('homepage');
    }

    public function sendForValidationAction($templateId)
    {
        $template = $this->getTemplate($templateId);

        if ($template->getStatus() === Template::STATUS_EDITABLE) {
            $template->setStatus(Template::STATUS_LOCK);
            $this->get('doctrine_mongodb')->getManager()->flush();

            return $this->redirectToRoute('homepage');
        }

        $this->addFlash('error', 'This template wasn\'t editable');

        return $this->redirectToRoute('homepage');
    }

    public function approveAction($templateId)
    {
        $template = $this->getTemplate($templateId);

        if ($template->getStatus() === Template::STATUS_LOCK) {
            $template->setStatus(Template::STATUS_VALIDATED);
            $this->get('doctrine_mongodb')->getManager()->flush();

            return $this->redirectToRoute('homepage');
        }

        $this->addFlash('error', 'This template wasn\'t locked');

        return $this->redirectToRoute('homepage');
    }

    public function rejectAction($templateId)
    {
        $template = $this->getTemplate($templateId);

        if ($template->getStatus() === Template::STATUS_LOCK) {
            $template->setStatus(Template::STATUS_EDITABLE);
            $this->get('doctrine_mongodb')->getManager()->flush();

            return $this->redirectToRoute('homepage');
        }

        $this->addFlash('error', 'This template wasn\'t locked');

        return $this->redirectToRoute('homepage');
    }

    /**
     * @param $id
     *
     * @return RedirectResponse|Response
     */
    public function viewAction($id)
    {
        $template = $this->getTemplate($id);

        return $this->get('node_html_render')->getNodeRenderResponse($template->getAlphaNode());
    }

    /**
     * @param string $id
     * @param string $format
     *
     * @return BinaryFileResponse|RedirectResponse
     */
    public function printAction($id, $format)
    {
        $template = $this->getTemplate($id);

        return $this->get('html_converter')->renderAsResponse(
            $this->get('node_html_render')->getNodeRenderResponse($template->getAlphaNode())->getContent(),
            $format,
            (string) $template->getId().'.'.$format
        );
    }

    /**
     * @param string $id
     *
     * @return RedirectResponse
     */
    public function removeAction($id)
    {
        $dm = $this->get('doctrine_mongodb')->getManager();

        $template = $this->getTemplate($id);

        $dm->remove($template);
        $dm->flush();

        return $this->redirect($this->generateUrl('homepage'));
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function searchAction(Request $request)
    {
        $filters = $request->get('filters');

        $dm = $this->get('doctrine_mongodb')->getManager();

        $findByArray = ['status' => Template::STATUS_VALIDATED];

        if (isset($filters['domain'])) {
            $domain                = $dm->getRepository('AppBundle:Domain')->find($filters['domain']['id']);
            $findByArray['domain'] = $domain;
        }

        if (isset($filters['category'])) {
            $category                = $dm->getRepository('AppBundle:Category')->find($filters['category']['id']);
            $findByArray['category'] = $category;
        }

        if (isset($filters['type'])) {
            $type                = $dm->getRepository('AppBundle:Type')->find($filters['type']['id']);
            $findByArray['type'] = $type;
        }

        $templates = $this->get('template_repository')->findBy($findByArray);

        return $this->render('AppBundle:template:search.html.twig', ['templates' => $templates]);
    }

    /**
     * @param $templateId
     *
     * @return Template
     */
    protected function getTemplate($templateId)
    {
        return $this->get('app_get_by_id')->getTemplate($templateId);
    }
}
