<?php

namespace AppBundle\Controller;

use AppBundle\Document\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class DashboardController extends Controller
{

    /**
     * @return Response
     */
    public function indexAction()
    {
        $fillings = $this->get('filling_repository')->findBy(['filler' => $this->getUser()], ['updateDate' => 'DESC']);

        return $this->render(
            'AppBundle:dashboard:index.html.twig',
            [
                'fillings' => $fillings,
            ]
        );
    }

    /**
     * @return Response
     */
    public function myTemplateAction()
    {
        $templatesEditable = $this->findTemplatesBy(['status' => Template::STATUS_EDITABLE]);

        return $this->render(
            'AppBundle:dashboard:my-template.html.twig',
            ['templatesEditable' => $templatesEditable]
        );
    }

    /**
     * @return Response
     */
    public function availableDocumentAction()
    {
        $templatesValidated = $this->findTemplatesBy(['status' => Template::STATUS_VALIDATED]);

        return $this->render(
            'AppBundle:dashboard:available-documents.html.twig',
            ['templatesValidated' => $templatesValidated]
        );
    }

    /**
     * @return Response
     */
    public function inValidationTemplateAction()
    {
        $templatesToValidate = $this->findTemplatesBy(['status' => Template::STATUS_LOCK]);

        return $this->render(
            'AppBundle:dashboard:in-validation-template.html.twig',
            ['templatesToValidate' => $templatesToValidate,]
        );
    }

    /**
     * @param array $criteria
     *
     * @return Template[]
     */
    private function findTemplatesBy(array $criteria)
    {
        if (!isset($criteria['owner'])) {
            $criteria['owner'] = $this->getUser();
        }

        return $this->get('template_repository')->findBy($criteria);
    }
}
