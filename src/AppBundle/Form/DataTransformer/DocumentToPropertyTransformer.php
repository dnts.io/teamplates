<?php

namespace AppBundle\Form\DataTransformer;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\PropertyAccess\PropertyAccess;

/**
 * Classe DocumentToPropertyTransformer
 *
 * @package AppBundle\Services
 */
class DocumentToPropertyTransformer implements DataTransformerInterface
{
    /** @var EntityManagerInterface */
    protected $em;
    /** @var  string */
    protected $className;
    /** @var  string */
    protected $textProperty;
    /** @var  string */
    protected $primaryKey;

    /**
     * @param ObjectManager $em
     * @param string        $class
     * @param string|null   $textProperty
     * @param string        $primaryKey
     */
    public function __construct(ObjectManager $em, $class, $textProperty = 'name', $primaryKey = 'id')
    {
        $this->em           = $em;
        $this->className    = $class;
        $this->textProperty = $textProperty;
        $this->primaryKey   = $primaryKey;
    }

    /**
     * Transform entity to array
     *
     * @param mixed $entity
     *
     * @return array
     */
    public function transform($entity)
    {

        $data = array();
        if (empty($entity) || $entity instanceof ArrayCollection) {
            return $data;
        }


        $accessor = PropertyAccess::createPropertyAccessor();

        $text = is_null($this->textProperty)
            ? (string) $entity
            : $accessor->getValue($entity, $this->textProperty);

        $data[$accessor->getValue($entity, $this->primaryKey)] = $text;

        return $data;
    }

    /**
     * Transform single id value to an entity
     *
     * @param string $value
     *
     * @return mixed|null|object
     */
    public function reverseTransform($value)
    {

        if (empty($value)) {
            return null;
        }


        try {

            $entity = $this->em->find($this->className, $value);

        } catch (\Exception $ex) {
            // this will happen if the form submits invalid data
            throw new TransformationFailedException(
                sprintf('The choice "%s" does not exist or is not unique "%s"', $value, $this->className)
            );
        }

        if (!$entity) {
            return null;
        }


        return $entity;
    }
}
