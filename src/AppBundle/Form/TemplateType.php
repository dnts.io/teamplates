<?php

namespace AppBundle\Form;

use AppBundle\Document\Category;
use AppBundle\Document\Domain;
use AppBundle\Document\Keyword;
use AppBundle\Document\Type;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\LanguageType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class TemplateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('shortDescription')
            ->add('language', LanguageType::class, [
                'preferred_choices' => ['fr', 'nl', 'en']
            ])
            ->add('version')
            ->add('internalReference')
            ->add('externalReference')
            ->add('comment')
            ->add('domain', Select2EntityType::class, [
                'remote_route' => 'template_attribute_search',
                'remote_params' => ['type' => 'domain'],
                'class' => Domain::class,
                'transformer' => 'AppBundle\Form\DataTransformer\DocumentToPropertyTransformer',
                'placeholder' => 'template.searchDomain',
            ])
            ->add('type', Select2EntityType::class, [
                'remote_route' => 'template_attribute_search',
                'remote_params' => ['type' => 'type'],
                'class' => Type::class,
                'transformer' => 'AppBundle\Form\DataTransformer\DocumentToPropertyTransformer',
                'placeholder' => 'template.searchType',
            ])
            ->add('category', Select2EntityType::class, [
                'remote_route' => 'template_attribute_search',
                'remote_params' => ['type' => 'category'],
                'class' => Category::class,
                'transformer' => 'AppBundle\Form\DataTransformer\DocumentToPropertyTransformer',
                'placeholder' => 'template.searchCategory',
            ])
            ->add('keywords', Select2EntityType::class, [
                'multiple' => true,
                'remote_route' => 'template_attribute_search',
                'remote_params' => ['type' => 'keyword'],
                'class' => Keyword::class,
                'transformer' => 'AppBundle\Form\DataTransformer\DocumentsToPropertyTransformer',
                'placeholder' => 'template.searchKeyword',
            ])
            ->add('save', SubmitType::class, [
                "label"=>"form.save",
                'translation_domain' => 'messages'
            ]);
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'AppBundle\Document\Template',
            )
        );
    }
}
