<?php

namespace AppBundle\Form;

use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Routing\RouterInterface;

class NodeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $templateId = 0; // $options['data']['templateId'];

        $builder->add(
            'content',
            CKEditorType::class,
            [
                'config' => [
                    'filebrowserImageUploadHandler' => function (RouterInterface $router) use ($templateId) {
                        return $router->generate('cke_upload', ['template_id' => $templateId], true);
                    },
                ],
            ]
        );
    }
}
