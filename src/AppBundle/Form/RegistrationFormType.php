<?php

namespace AppBundle\Form;

use AppBundle\Document\Company;
use Doctrine\ODM\MongoDB\DocumentRepository;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

class RegistrationFormType extends BaseType
{
    /** @var DocumentRepository */
    private $companyRepository;

    /**
     * RegistrationFormType constructor.
     *
     * @param string             $class
     * @param DocumentRepository $companyRepository
     */
    public function __construct($class, DocumentRepository $companyRepository)
    {
        parent::__construct($class);
        $this->companyRepository = $companyRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $companies = [];
        /** @var Company[] $allCompanies */
        $allCompanies = $this->companyRepository->findAll();
        foreach ($allCompanies as $company) {
            $companies[$company->getName()] = $company;
        }

        $builder->add('company', ChoiceType::class, ['choices' => $companies]);
    }

    public function getName()
    {
        return 'fp_user_registration';
    }
}
