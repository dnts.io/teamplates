<?php

namespace AppBundle\Form;

use AppBundle\Document\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserRoleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'roles',
                ChoiceType::class,
                array(
                    'choices'  =>
                        array(
                            'ROLE_VALIDATOR'  => 'ROLE_VALIDATOR',
                            'ROLE_TEMPLATER'  => 'ROLE_TEMPLATER',
                            'ROLE_ADMIN' => 'ROLE_ADMIN',
                        ),
                    'required' => true,
                    'multiple' => true,
                    // 'multiple' => true,
                     'expanded' => true,
                )
            )
            ->add('add', SubmitType::class);
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => User::class,
            )
        );
    }
}
