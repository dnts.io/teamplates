module.exports = {
    files: {
        javascripts: {
            joinTo: {
                'app.js': /^(src|node_modules)/
            }
        },
        stylesheets: {
            joinTo: 'app.css'
        }
    },
    plugins: {
        babel: {
            presets: ['latest']
        },
        cleancss: {
            removeEmpty: true,
            keepSpecialComments: 0,
            restructure: true
        }
    },
    paths: {
        // Change the "public" path (where the build will go)
        "public": 'web/',
        // We change the "app" path
        'watched': ['src/AppBundle/Resources/public']
    },
    conventions: {
        // With this Brunch will copy all folder in this folder without touching them (img, font, ...)
        'assets': /^src\/AppBundle\/Resources\/public\/assets/
    },
    npm: {
        // Some of our plugins require to be global
        globals: {
            $: 'jquery',
            jQuery: 'jquery',
            jQueryUi: 'jqueryui'
        },
        styles: {
            select2: ['dist/css/select2.css'],
            'jquery.fancytree': ['dist/skin-lion/ui.fancytree.css'],
        }
    },
    modules: {
        // This will allow us to require/import JS file without specify ALL the path
        nameCleaner: function (path) {
            return path.replace(/^src\/AppBundle\/Resources\/public\//, '');
        }
    },
    hooks: {
        preCompile: (end) => {
            console.log("About to compile...");
            var fs = require('fs'),
                publicVendorPath = __dirname + '/web/vendor/';

            if (!fs.existsSync(publicVendorPath)) {
                fs.mkdirSync(publicVendorPath);
            }

            fs.createReadStream(__dirname + '/node_modules/jquery.fancytree/dist/jquery.fancytree-all.min.js').pipe(fs.createWriteStream(publicVendorPath + 'jquery.fancytree-all.min.js'));

            end();
        }
    }
};